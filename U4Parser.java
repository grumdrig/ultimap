import java.io.*;
import java.awt.image.BufferedImage;


class U4Parser extends UltimaParser {
  // Big ups to the folks at www.moongates.com for exposing the Ultima IV file formats.

  protected String getDataDir() { return "ULTIMA4/"; }

  private static final String tileDataFilename = "SHAPES.EGA";
  protected String getTileFilename() { return "u4tiles.png"; }
  //private static final String tileDataFilename = "egatiles.bin";
  //private static final String tileFilename = "u1tiles.png";
  // (an experiment on the U1 tiles)

  private static final String tileCGADataFilename = "SHAPES.CGA";
  private static final String tileCGAFilename = "u4cga.png";

  private static final String fontDataFilename = "CHARSET.EGA";
  protected String getFontFilename() { return "u4font.png"; }

  private static final String fontCGADataFilename = "CHARSET.CGA";
  private static final String fontCGAFilename = "u4cgaf.png";

  private static final String worldDataFilename = "WORLD.MAP";
  private static final String worldStitchedFilename = "u4world.png";
	private static final String worldChunkFormat = "u4world%02o.png";
	private static final String worldMinimapFilename = "u4minimap.png";

  private static final String towneDataExtension = ".ULT";
  private static final String towneFormat = "u4map%S.png";
  private static final String towneSpawnFormat = "u4map%Ss.png";

  private static final String talkDataExtension = ".TLK";
  private static final String talkFormat = "u4tlk%S.txt";

  private static final String conflictDataExtension = ".CON";
  private static final String conflictFormat = "u4con%S.png";
  private static final String conflictSpawnFormat = "u4con%Ss.png";

  private static final String dungeonDataExtension = ".DNG";
  private static final String dungeonFormat = "u4dng%S%d.png";
  private static final String dungeonRoomFormat = "u4dng%Sr%02d.png";
  private static final String dungeonRoomSpawnFormat = "u4dng%Sr%02ds.png";
  private static final String dungeonRoomTriggerFormat = "u4dng%Sr%02dt.png";


  private void parseConflict(final String fileName) {
    // There is a CAMP.DNG but it ain't no dungeon as far as I can tell.
    if (fileName.equals("CAMP")) {
      System.out.println("Whatever CAMP is, it's not a dungeon file I can process.");
      System.exit(0);
    }

    // Get source bitmap
    BufferedImage tiles = getTileImages();

    // First 64 bytes of data are header (unless this is a shrine file)
    int headerSize = 64;
    if (fileName.equals("SHRINE"))
      headerSize = 0;

    // Get data
    final String dataFileName = fileName + conflictDataExtension;
    byte[] data = new byte[headerSize + 121];
    // (expect that there are exactly 7 more bytes which are always a certain value)
    getData(data, getInputStream(dataFileName), true);

    // Prepare the image we will draw to - always 11x11 tiles
		final int FIELD_SIZE = 11;
    BufferedImage image = new BufferedImage(FIELD_SIZE * TILE_SIZE,
																						FIELD_SIZE * TILE_SIZE,
																						BufferedImage.TYPE_INT_RGB);
    parseTiles(tiles, image, data, headerSize, FIELD_SIZE);

    int i = 0;
    int tileX = 0, tileY = 0;
    int mapX = 0, mapY = 0;

    // Save image file
    String outFileName = String.format(conflictFormat, fileName);
    outFileName = outFileName.toLowerCase();
    writeImage(image, outFileName);

    // Now go back to header and make another image with the spawn data
    int x = 0, y = 0;
    if (headerSize > 0) {
      // Get font images
      BufferedImage font = getFontImages();

      U4Data parseData = new U4Data();

      // Monster spawn data
      tileX = (parseData.TILE_DOT & 0x0F) * TILE_SIZE;
      tileY = (parseData.TILE_DOT >> 4) * TILE_SIZE;
      for (i = 0; i < TILE_SIZE; ++i) {
        x = data[i];
        y = data[i + TILE_SIZE];
        mapX = x * TILE_SIZE;
        mapY = y * TILE_SIZE;
        copyMaskedTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);
      }

      // PC spawn data - 8 party slots
      tileX = (parseData.TILE_BOX & 0x0F) * TILE_SIZE;
      tileY = (parseData.TILE_BOX >> 4) * TILE_SIZE;
      int fontX = 0;
      int fontY = (parseData.FONT_1 >> 4) * FONT_SIZE;
      for (i = 0; i < 8; ++i) {
        x = data[i+32];
        y = data[i+40];
        mapX = x * TILE_SIZE;
        mapY = y * TILE_SIZE;
        copyMaskedTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);

        fontX = (((parseData.FONT_1 & 0x0F) + i) * FONT_SIZE);
        // (index # looks slightly better just right of center, IMO)
        copyMaskedTile(font, image, fontX, fontY, mapX + 5, mapY + 4, FONT_SIZE);
      }
	    
      // (there are also 16 more bytes which expect to have exact value)

      // Save image file
      outFileName = String.format(conflictSpawnFormat, fileName);
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
    }
  }

  private void parseDungeon(final String fileName) {
    // Get source images
    BufferedImage tiles = getTileImages();
    BufferedImage font = getFontImages();
	
    // Abyss file is markedly larger than the rest
    int fileSize = 4608;
    if (fileName.equals("ABYSS"))
      fileSize = 16896;
	
    // Get data
    final String dataFileName = fileName + dungeonDataExtension;
    byte[] data = new byte[fileSize];
    getData(data, getInputStream(dataFileName), true);

    // There are special codes in dungeon maps that go back to map tiles
    final int dungeonRoom = 0xD0; // detect D0-DF w/ fewer entries
    U4Data parseData = new U4Data();
    parseData.buildDungeonData();

    // There are eight floors to each dungeon
    int i = 0;
    int datum = 0;
    Short tile = null;
    int tileX = 0, tileY = 0;
    int mapX = 0, mapY = 0;
		final int FLOORS = 8; // 8 floors per dungeon
		final int FLOOR_SIZE = 8; // and each floor is 8x8
		final int ROOMS = 16; // 16 rooms per standard dungeon
    for (int level = 0; level < FLOORS; ++level) {
      // Prepare the image we will draw floor to - always 8x8 tiles
      BufferedImage image = new BufferedImage(FLOOR_SIZE * TILE_SIZE,
                                              FLOOR_SIZE * TILE_SIZE,
                                              BufferedImage.TYPE_INT_RGB);
      for (i = 0; i < FLOOR_SIZE * FLOOR_SIZE; ++i) {
        datum = unsign(data[level * (FLOOR_SIZE * FLOOR_SIZE) + i]);

        // Turn the special dungeon code into the regular tile map code
        tile = parseData.getDungeonTile(datum);
        if (tile == null) {
          // This had better be a dungeon room! (16 codes for those)
          if ((datum > 0xD0) && (datum - 0xD0 < ROOMS)) {
            tile = parseData.getDungeonTile(dungeonRoom);
          } 
          else {
            System.err.println(String.format("Unexpected dungeon map datum %X on level %d", datum, level + 1));
            System.exit(1);
          }
        }

        // Put the tile on the map
        tileX = (tile.shortValue() & 0x0F) * TILE_SIZE;
        tileY = (tile.shortValue() >> 4) * TILE_SIZE;
        mapX = (i % FLOOR_SIZE) * TILE_SIZE;
        mapY = (i / FLOOR_SIZE) * TILE_SIZE;
        copyTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);

        // If there's a letter that should be drawn on top, do that
        tile = parseData.getDungeonMark(datum);
        if (tile == null) {
          // Handling the dungeon rooms special - check if we've got one
          int diff = (datum - 0xD0) + 1;
          if ((datum >= 0xD0) && (diff <= ROOMS)) {
            // ABYSS has many more rooms than the other dungeons
            if (fileName.equals("ABYSS")) {
              if (level > 1) diff += ROOMS;
              if (level > 3) diff += ROOMS;
              if (level > 5) diff += ROOMS;
            }

            // Write as two-digit number for simplicity
            tileX = (parseData.FONT_0 & 0x0F) * FONT_SIZE;
            tileY = (parseData.FONT_0 >> 4) * FONT_SIZE;
            mapY += FONT_SIZE >> 1;
            copyMaskedTile(font,
													 image,
													 tileX + (diff / 10) * FONT_SIZE,
													 tileY,
													 mapX,
													 mapY,
													 FONT_SIZE);
            copyMaskedTile(font,
													 image,
													 tileX + (diff % 10) * FONT_SIZE,
													 tileY,
													 mapX + FONT_SIZE,
													 mapY,
													 FONT_SIZE);
          }
        } else {
          tileX = (tile.shortValue() & 0x0F) * FONT_SIZE;
          tileY = (tile.shortValue() >> 4) * FONT_SIZE;
          mapX += FONT_SIZE >> 1;
          mapY += FONT_SIZE >> 1;
          copyMaskedTile(font, image, tileX, tileY, mapX, mapY, FONT_SIZE);
        }
      }

      // Save image file
      String outFileName = String.format(dungeonFormat, fileName, level + 1);
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
      System.out.print(".");
    }

    // In drawing rooms, will often draw some boxes, so let's just cook that up once.
    final int boxX = (parseData.TILE_BOX & 0x0F) * TILE_SIZE;
    final int boxY = (parseData.TILE_BOX >> 4) * TILE_SIZE;

    // There are either 16 or 64 dungeon rooms following.  Just trust data length.
    // (also expect 7 padding bytes at the end, all = 0)
    int j = 0, k = 0;
    int roomNum = 1;
		final int ROOM_SIZE = 11;
    for (i = FLOORS * (FLOOR_SIZE * FLOOR_SIZE);
				 i < data.length - 7;
				 i += 256, roomNum++) {
      // Prepare the image we will draw to - always 11x11 tiles
      BufferedImage image = new BufferedImage(ROOM_SIZE * TILE_SIZE,
																							ROOM_SIZE * TILE_SIZE,
																							BufferedImage.TYPE_INT_RGB);

      // First skip ahead to the floorplan - standard tile data
      int skip = 16 + (16 + 16 + 16) + (8 + 8) + (8 + 8) + (8 + 8) + (8 + 8);
      parseTiles(tiles, image, data, i + skip, 11);

      // Save image file - the empty room/floor plan
      String outFileName = String.format(dungeonRoomFormat, fileName, roomNum);
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
      System.out.print(".");

      // If there is any trigger data, make an image to contain that.
      // But oftentimes there are no triggers, so skip this if you find none
      skip = 0;

      // Word has it that if there are any triggers, they come first in the list.
      // There has to be an elegant way of doing this, but for now...
      if( unsign(data[skip + i + 0]) != 0 ||
          unsign(data[skip + i + 1]) != 0 ||
          unsign(data[skip + i + 2]) != 0 ||
          unsign(data[skip + i + 3]) != 0 ) {
        // At least one trigger exists.
        int triggerX = 0, triggerY = 0;
		
        // Create a separate image for the trigger map (will reuse orig for spawn map)
        BufferedImage triggerImage = new BufferedImage(image.getWidth(),
																											 image.getHeight(),
																											 image.getType());
        copyTile(image, triggerImage, 0, 0, 0, 0, ROOM_SIZE * TILE_SIZE);
	    
        for (j = 0; j < 16; j += 4) {
          // The trigger changes tiles to another kind
          datum = unsign(data[(skip + i) + j]);
          tileY = (datum >> 4) * TILE_SIZE;
          tileX = (datum & 0x0F) * TILE_SIZE;

          // The trigger is located somewhere on map
          datum = unsign(data[(skip + i) + j + 1]);
          // Only here did it occur to Origin to pack 11x11 coords into 1 byte.
          triggerX = (datum >> 4) * TILE_SIZE;
          triggerY = (datum & 0x0F) * TILE_SIZE;

          //** Some triggers reverse what other triggers do.
          // That's overwritten here, and whatever trigger is listed last wins
          // the battle for visibility.  Pink boxes will mitigate that effect.

          if (triggerX != 0 || triggerY != 0 || tileX != 0 || tileY != 0) {
            // or, theoretically, one of the following bytes, but I'll doom the
            // case where the NW corner of screen creates deep ocean for the moment
		    
            // The trigger itself
            copyMaskedTile(tiles,
													 triggerImage,
													 (parseData.TILE_DOT & 0x0F) * TILE_SIZE,
													 (parseData.TILE_DOT >> 4) * TILE_SIZE,
													 triggerX,
													 triggerY,
													 TILE_SIZE);

            // The affected tile(s).  Sometimes only one of the fields seems
            // to be used, so I am taking a 0,0 value to mean no change.
            // (Seems they might have chosen to count from 1 for this reason...)
            for (k = 2; k <= 3; k++) {
              datum = unsign(data[(skip + i) + j + k]);
              mapX = (datum >> 4) * TILE_SIZE;
              mapY = (datum & 0x0F) * TILE_SIZE;
              if (mapX != 0 || mapY != 0) {
                copyTile(tiles, triggerImage, tileX, tileY, mapX, mapY, TILE_SIZE);
                copyMaskedTile(tiles, triggerImage, boxX, boxY, mapX, mapY, TILE_SIZE);
                // Maybe a nice little dotted line? overkill I reckon
              }
            }
          }
        }

        // Save trigger file
        outFileName = String.format(dungeonRoomTriggerFormat, fileName, roomNum);
        outFileName = outFileName.toLowerCase();
        writeImage(triggerImage, outFileName);
        System.out.print(".");
      }

      // Now image can get the monster/player spawn data.
      // Starting with monsters:
      skip = 16;
      for (j = 0; j < 16; ++j) {
        datum = unsign(data[i + j + skip]);
        // Empty monster slots appear first (and aren't deep ocean)
        if (datum != 0) {
          tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
          tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
          mapX = (data[i + j + skip + 16]) * TILE_SIZE;
          mapY = (data[i + j + skip + 32]) * TILE_SIZE;
          copyTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);

          // I had thought there was a chance that some monsters overlapped.
          // Then I wrote a bunch of code to indicate those cases,
          // and then I decided I was wrong.  Now there is this comment.
        }
      }

      // And finishing up with the nice guys (8 party slots):
      skip = 16 + (16 + 16 + 16);
      tileX = (parseData.FONT_1 & 0x0F) * FONT_SIZE;
      tileY = (parseData.FONT_1 >> 4) * FONT_SIZE;
      for (j = 0; j < 8; ++j) {
        // There are 4 entry points for each character, for each entrance
        for (k = 0; k < 4; ++k) {
          mapX = (data[(skip + i) + j + (k * 16)]) * TILE_SIZE;
          mapY = (data[(skip + i) + (j + 8) + (k * 16)]) * TILE_SIZE;

          // When there's no expectation they'll come in some way, data's 0,0.
          // I'll trust that no individual is ever expected to enter from there.
          if (mapX != 0 || mapY != 0) {
            copyMaskedTile(tiles, image, boxX, boxY, mapX, mapY, TILE_SIZE);
            // (index # looks slightly better just right of center, IMO)
            copyMaskedTile(font, image, tileX, tileY, mapX+5, mapY+4, FONT_SIZE);
          }
        }
        tileX += FONT_SIZE;
      }

      // Save spawn file
      outFileName = String.format(dungeonRoomSpawnFormat, fileName, roomNum);
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
      System.out.print(".");
    }
    System.out.println();
  }

  //** this could probably be moved to UltimaParser thru use of filename
  // args and putting palette stuff into data
  private void parseWorld(final boolean stitch,
                          final boolean generateJs) throws IOException {
    // Get tile images
    BufferedImage tiles = null;
    if (stitch) tiles = getTileImages();

    // Open the file...
    byte[] data = new byte[MAP_SIZE * MAP_SIZE];
    getData(data, getInputStream(worldDataFilename), true);

    // Either make a gigantic image or don't
    BufferedImage image = null;
    Writer worldJs = null;
    if (stitch) {
      image = new BufferedImage(MAP_SIZE * TILE_SIZE, 
                                MAP_SIZE * TILE_SIZE, 
                                BufferedImage.TYPE_INT_RGB);
    } else {
      image = new BufferedImage(MAP_SIZE, MAP_SIZE,
                                BufferedImage.TYPE_INT_RGB);
    }

    if (generateJs) {
      worldJs = new BufferedWriter(new FileWriter("u4world.js"));
      worldJs.write("var map4=[\n");
    }

    for (int mapY = 0, index = 0; mapY < MAP_SIZE; ++mapY) {
      if (worldJs != null)
        worldJs.write("[");
      for (int mapX = 0; mapX < MAP_SIZE; ++mapX, ++index) {
        int datum = unsign(data[unchunk(index)]);
        int tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
        int tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
        if (stitch) {
          copyTile(tiles, image, 
                   tileX, tileY, 
                   mapX * TILE_SIZE, mapY * TILE_SIZE, 
                   TILE_SIZE);
        } else {
          final int terraColors[] = {
            0x000050,  // deep blue sea
            0x0000A0,  // blue sea
            0x4040A0,  // shallows
            0x004080,  // marsh
            0x000E00,  // grass
            0x004700,  // brush
            0x408300,  // forest
            0x444844,  // hills
            0x8F8F8E,  // mountains
          };
          int color;
          if (datum < terraColors.length) 
            color = terraColors[datum];
          else if (datum == 70)  // a'a
            color = 0xFF0000;
          else if (datum == 76)  // pahoehoe
            color = 0x800000;
          else                   // town or some shit
            color = 0xFFFFFF;
          image.setRGB(mapX, mapY, color);
        }

        if (worldJs != null)
          worldJs.write(datum + ",");
      }
      if (worldJs != null)
        worldJs.write("],\n");
      System.out.print(".");
    }

    if (stitch) {
      writeImage(image, worldStitchedFilename);
    } else {
      writeImage(image, worldMinimapFilename);
    }
    if (worldJs != null) {
      worldJs.write("];\n");
      worldJs.close();
    }

    System.out.println(".");
  }

  // This serves no practical purpose but I like to see 'em.
  private void parseWorldChunks() {
    // Get tile images
    BufferedImage tiles = getTileImages();
    
    // Open the file...
    byte[] data = new byte[MAP_SIZE * MAP_SIZE];
    getData(data, getInputStream(worldDataFilename), true);
    
    // The map is arranged in 64 chunks of 32x32 tiles
    int datum = 0;
    int i = 0;
    int tileX = 0, tileY = 0;
    int mapX, mapY = 0;
    for (int chunk = 0; chunk < 64; ++chunk) {
      // Prepare the image we will draw to - always 32x32 tiles
      BufferedImage image = new BufferedImage(CHUNK_SIZE * TILE_SIZE,
                                              CHUNK_SIZE * TILE_SIZE,
                                              BufferedImage.TYPE_INT_RGB);
      
      for (i = 0; i < CHUNK_SIZE * CHUNK_SIZE; ++i) {
        datum = unsign(data[i + chunk * (CHUNK_SIZE * CHUNK_SIZE)]);
        
        tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
        tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
        mapX = (i % CHUNK_SIZE) * TILE_SIZE;
        mapY = (i / CHUNK_SIZE) * TILE_SIZE;
        
        copyTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);
      }
      
      // Save image file
      String outFileName = String.format(worldChunkFormat, chunk);
      writeImage(image, outFileName);
      System.out.print(".");
    }
    System.out.println();
  }

  private void parseTowne(final String fileName) {
    // Get source images
    BufferedImage tiles = getTileImages();

    // Get data
    final String dataFileName = fileName + towneDataExtension;
    byte[] data = new byte[1280];
    getData(data, getInputStream(dataFileName), true);

    // Draw the map - always 32x32 tiles
		final int TOWN_SIZE = 32;
    BufferedImage image = new BufferedImage(TOWN_SIZE * TILE_SIZE,
                                            TOWN_SIZE * TILE_SIZE,
                                            BufferedImage.TYPE_INT_RGB);
    parseTiles(tiles, image, data, 0, TOWN_SIZE);

    // Save image file
    String outFileName = String.format(towneFormat, fileName);
    outFileName = outFileName.toLowerCase();
    writeImage(image, outFileName);

    // Now we show where the people appear (up to 32 of 'em)
    BufferedImage font = getFontImages();
    int datum = 0;
    int tileX = 0, tileY = 0;
    int mapX = 0, mapY = 0;
    U4Data parseData = new U4Data();
    //final int boxX = (parseData.TILE_BOX & 0x0F) * TILE_SIZE;
    //final int boxY = (parseData.TILE_BOX >> 4) * TILE_SIZE;
    final int fontX = (parseData.FONT_0 & 0x0F) * FONT_SIZE;
    final int fontY = (parseData.FONT_0 >> 4) * FONT_SIZE;
    for (int i = 0; i < 32; ++i) {
      datum = unsign(data[i + 1024 + 96]);
   
      // Any empties show up in the beginning
      if (datum != 0) {
        tileY = (datum >> 4) * TILE_SIZE;
        tileX = (datum & 0xF) * TILE_SIZE;
        mapX = unsign(data[i + 1024 + 96 + 32]) * TILE_SIZE;
        mapY = unsign(data[i + 1024 + 96 + 64]) * TILE_SIZE;
        // (Expect that there is a roughly identical set of bytes before these too.)
        copyTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);
        //copyMaskedTile(tiles, image, boxX, boxY, mapX, mapY, TILE_SIZE);

        // There is also a behavior ID in between - stationary, wander, track, attack

        // And now show 'em their talk ID
        datum = unsign(data[i + 1024 + 96 + 128]);
        if (datum >= 10)
          copyMaskedTile(font,
												 image,
												 fontX + (datum / 10) * FONT_SIZE,
												 fontY,
												 mapX,
												 mapY + (TILE_SIZE >> 1),
												 FONT_SIZE);
        copyMaskedTile(font,
											 image,
											 fontX + (datum % 10) * FONT_SIZE,
											 fontY,
											 mapX + FONT_SIZE,
											 mapY + (TILE_SIZE >> 1),
											 FONT_SIZE);
      }
    }

    // Save spawn image file
    outFileName = String.format(towneSpawnFormat, fileName);
    outFileName = outFileName.toLowerCase();
    writeImage(image, outFileName);
  }

  private static void printYNQuestion(PrintWriter out,
                                      final String question,
                                      final String yesResponse,
                                      final String noResponse,
                                      final boolean humility) {
    if (humility)
      out.print("!H!");
    out.println(String.format("*:\t%s\n*Y:\t%s\n*N:\t%s",
                              question,
                              yesResponse,
                              noResponse));
  }

  private void parseTalk(final String fileName) {
    // Get the data file
    byte[] data = new byte[4608];
    getData(data, getInputStream(fileName + talkDataExtension), true);

    // Open a text file
    String outFileName = String.format(talkFormat, fileName);
    outFileName = outFileName.toLowerCase();
    FileWriter outFile = null;
    try {
      outFile = new FileWriter(outFileName, false);
    } catch (IOException except) {
      System.err.println("Error opening file " + outFileName);
      System.exit(1);
    }
    PrintWriter out = new PrintWriter(outFile);

    // Collect data for each entry
    int yesNoTrigger = 0, turnAway = 0;
    boolean humility = false;
    String[] fields = new String[12];
    final short
      NAME = 0,
      PRONOUN = 1,
      LOOK = 2,
      JOB = 3,
      HEALTH = 4,
      KEY1R = 5,
      KEY2R = 6,
      YNQ = 7,
      YR = 8,
      NR = 9,
      KEY1 = 10,
      KEY2 = 11;
    char datum = 0;
    int field = 0;
    int id = 1;
    for (int i = 0; i < data.length; i += 288, id++) {
      //System.out.println(i);
      int j = i;
      field = 0;

      yesNoTrigger = unsign(data[j++]);
      // 0-none, 3-job, 4-health, 5-key1, 6-key2

      humility = (unsign(data[j++]) != 0);
      turnAway = unsign(data[j++]);

      StringBuilder notepad = new StringBuilder();
      while (j < 285 + i && field < fields.length) {
        datum = (char)unsign(data[j++]);
        if (datum == 0) {
          // Field is terminated
          //System.out.println(field + " " + j + " " + notepad.toString());
          fields[field++] = notepad.toString();
          if (field < fields.length)
            notepad = new StringBuilder();
        }
        else if (datum == 0x0A) {
          // These are explicit linebreaks
          //notepad.append('\n');
          // But we have a few more columns at our disposal
          notepad.append(' ');
        }
        else {
          notepad.append(datum);
        }
      }

      // Write data - values "A" and "A   " mean "not applicable"
      out.println(String.format("#%d   (%s)", id, fields[PRONOUN]));
      out.println(String.format("%s, %s", fields[NAME], fields[LOOK]));

      if (yesNoTrigger == 3) out.print("*");
      if (!fields[JOB].equals("A"))
        out.println(String.format("JOB:\t%s", fields[JOB]));
      if (yesNoTrigger == 3)
        printYNQuestion(out, fields[YNQ], fields[YR], fields[NR], humility);

      if (yesNoTrigger == 4) out.print("*");
      if (!fields[HEALTH].equals("A"))
        out.println(String.format("HEALTH:\t%s", fields[HEALTH]));
      if (yesNoTrigger == 4)
        printYNQuestion(out, fields[YNQ], fields[YR], fields[NR], humility);

      if (yesNoTrigger == 5) out.print("*");
      if (!fields[KEY1].equals("A   "))
        out.println(String.format("%s:\t%s", fields[KEY1], fields[KEY1R]));
      if (yesNoTrigger == 5)
        printYNQuestion(out, fields[YNQ], fields[YR], fields[NR], humility);

      if (yesNoTrigger == 6) out.print("*");
      if (!fields[KEY2].equals("A   "))
        out.println(String.format("%s:\t%s", fields[KEY2], fields[KEY2R]));
      if (yesNoTrigger == 6)
        printYNQuestion(out, fields[YNQ], fields[YR], fields[NR], humility);

      if (turnAway > 0)
        out.println(String.format("Turn=%d\t(Turns away ~%.0f%% of the time.)",
                                  turnAway,
                                  (float)(turnAway)/2.550));

      out.println();
    }

    out.close();
  }

  private static void printInstructions() {
		PrintStream o = System.out;
    o.println("U4Parser function depends on option given:");
    o.println("  U4Parser -g : Generates graphics tile bitmap from SHAPES.EGA");
    o.println("  U4Parser -f : Generates font bitmap from CHARSET.EGA");
    o.println("  U4Parser -mini : * Generates minimap from WORLD.MAP");
    o.println("  U4Parser -w : * Generates Javascript describing world map");
    o.println("  U4Parser -ws : * Generates bitmap of world map from WORLD.MAP (requires large heap)");
    o.println("  U4Parser -wc : * Generates several bitmaps of world map chunks from WORLD.MAP");
    o.println("  U4Parser -c [FILE] : * Generates bitmaps of conflict map from [FILE].CON");
    o.println("  U4Parser -d [FILE] : * Generates bitmaps of dungeon map from [FILE].DNG");
    o.println("  U4Parser -m [FILE] : * Generates bitmaps of towne map from [FILE].ULT");
    o.println("  U4Parser -t [FILE] : Generates text file of talk file from [FILE].TLK");
    o.println(" * : These options require the bitmaps from -g and/or -f options");
  }

  private boolean eatArgs(String[] args) throws IOException {
    if (eatTestingArgs(args))
      return true;

    final String modeArg = args[0].toLowerCase();
    if (modeArg.equals("-g"))
      parseEga(tileDataFilename, getTileFilename(), 256, TILE_SIZE, 0);
    else if (modeArg.equals("-cga")) // MERRY EASTER!!!
      parseCga(tileCGADataFilename, tileCGAFilename, 256, TILE_SIZE, true);
    else if (modeArg.equals("-f"))
      parseEga(fontDataFilename, getFontFilename(), 128, FONT_SIZE, 0);
    else if (modeArg.equals("-cgaf")) // etc
      parseCga(fontCGADataFilename, fontCGAFilename, 128, FONT_SIZE, true);
    else if (modeArg.equals("-c") && args.length > 1)
      parseConflict(args[1].toUpperCase());
    else if (modeArg.equals("-d") && args.length > 1)
      parseDungeon(args[1].toUpperCase());
    else if (modeArg.equals("-mini"))
      parseWorld(false, false);
    else if (modeArg.equals("-w"))
      parseWorld(false, true);
    else if (modeArg.equals("-ws"))
      parseWorld(true, false);
    else if (modeArg.equals("-wc"))
      parseWorldChunks();
    else if (modeArg.equals("-m") && args.length > 1)
      parseTowne(args[1].toUpperCase());
    else if (modeArg.equals("-t") && args.length > 1)
      parseTalk(args[1].toUpperCase());
    else
      return false;
    return true;
  }

  public static void main(String[] args) throws IOException {
    boolean eaten = false;
    if (args.length > 0)
      eaten = new U4Parser().eatArgs(args);
    if (!eaten)
      printInstructions();
  }
}
