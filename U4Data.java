class U4Data {
  // Big ups to the folks at www.moongates.com for exposing the
  // Ultima IV file formats.

  // Note the following hex codes can be read as Y,X tile-coordinates
  // into the texture map. Which I believe means they're the same as
  // what would be in raw data to identify tiles.
  public final short TILE_ALTAR = 0x4A;
  public final short TILE_BLACK = 0x7E;
  public final short TILE_BOX = 0x7D;
  public final short TILE_BRICK = 0x3E;
  public final short TILE_CHEST = 0x3C;
  public final short TILE_DOOR = 0x3B;
  public final short TILE_DOT = 0x4D;
  public final short TILE_FIELD_ENERGY = 0x45;
  public final short TILE_FIELD_FIRE = 0x46;
  public final short TILE_FIELD_POISON = 0x44;
  public final short TILE_FIELD_SLEEP = 0x47;
  public final short TILE_HEX = 0x16;
  public final short TILE_HIT = 0x4F;
  public final short TILE_LADDER_DOWN = 0x1C;
  public final short TILE_LADDER_UP = 0x1B;
  public final short TILE_MAGIC = 0x4E;
  public final short TILE_SECRET_DOOR = 0x49;
  public final short TILE_SHOALS = 0x02;
  public final short TILE_WALL = 0x7F;
  public final short TILE_WHITE = 0x48;

  public final short FONT_0 = 0x30;
  public final short FONT_1 = 0x31;
  public final short FONT_A = 0x41;
  public final short FONT_C = 0x43;
  public final short FONT_F = 0x46;
  public final short FONT_H = 0x48;
  public final short FONT_P = 0x50;
  public final short FONT_R = 0x52;
  public final short FONT_S = 0x53;
  public final short FONT_W = 0x57;
  public final short FONT_ARROW_BOTH = 0x04;
  public final short FONT_ARROW_DOWN = 0x05;
  public final short FONT_ARROW_UP = 0x06;
  public final short FONT_ASTERISK = 0x21;
  
  private java.util.HashMap<Integer,Short> dungeonTileData = null;
  private java.util.HashMap<Integer,Short> dungeonMarkData = null;

  public void buildDungeonData() {
    // There are special codes in dungeon maps that go back to map tiles
    if (dungeonTileData == null) {
	    //final Integer dungeonRoom = new Integer(0xD0); // detect D0-DF w/ fewer entries
	    dungeonTileData = new java.util.HashMap<Integer,Short>();
	    dungeonTileData.put(new Integer(0x00), new Short(TILE_HEX));
	    dungeonTileData.put(new Integer(0x10), new Short(TILE_LADDER_UP));
	    dungeonTileData.put(new Integer(0x20), new Short(TILE_LADDER_DOWN));
	    dungeonTileData.put(new Integer(0x30), new Short(TILE_LADDER_UP)); // both
	    dungeonTileData.put(new Integer(0x40), new Short(TILE_CHEST));
	    dungeonTileData.put(new Integer(0x50), new Short(TILE_DOT)); // ceiling hole
	    dungeonTileData.put(new Integer(0x60), new Short(TILE_DOT)); // floor hole
	    dungeonTileData.put(new Integer(0x70), new Short(TILE_MAGIC));
	    dungeonTileData.put(new Integer(0x80), new Short(TILE_BLACK)); // winds
	    dungeonTileData.put(new Integer(0x81), new Short(TILE_BLACK)); // rocks
	    dungeonTileData.put(new Integer(0x8E), new Short(TILE_BLACK)); // pit
	    // intended HIT for the traps but then P and R are too hard to tell apart
	    dungeonTileData.put(new Integer(0x90), new Short(TILE_SHOALS)); // plain
	    dungeonTileData.put(new Integer(0x91), new Short(TILE_SHOALS)); // heal
	    dungeonTileData.put(new Integer(0x92), new Short(TILE_SHOALS)); // acid
	    dungeonTileData.put(new Integer(0x93), new Short(TILE_SHOALS)); // cure
	    dungeonTileData.put(new Integer(0x94), new Short(TILE_SHOALS)); // poison
	    dungeonTileData.put(new Integer(0xA0), new Short(TILE_FIELD_POISON));
	    dungeonTileData.put(new Integer(0xA1), new Short(TILE_FIELD_ENERGY));
	    dungeonTileData.put(new Integer(0xA2), new Short(TILE_FIELD_FIRE));
	    dungeonTileData.put(new Integer(0xA3), new Short(TILE_FIELD_SLEEP));
	    dungeonTileData.put(new Integer(0xB0), new Short(TILE_ALTAR));
	    dungeonTileData.put(new Integer(0xC0), new Short(TILE_DOOR));
	    dungeonTileData.put(new Integer(0xD0), new Short(TILE_BOX)); // thru 0xDF
	    dungeonTileData.put(new Integer(0xE0), new Short(TILE_SECRET_DOOR));
	    dungeonTileData.put(new Integer(0xF0), new Short(TILE_WALL));
    }
    if (dungeonMarkData == null) {
	    // And to further disambiguate, some of those guys get a letter on top
	    dungeonMarkData = new java.util.HashMap<Integer,Short>();
	    dungeonMarkData.put(new Integer(0x10), new Short(FONT_ARROW_UP));
	    dungeonMarkData.put(new Integer(0x20), new Short(FONT_ARROW_DOWN));
	    dungeonMarkData.put(new Integer(0x30), new Short(FONT_ARROW_BOTH));
	    dungeonMarkData.put(new Integer(0x50), new Short(FONT_C));
	    dungeonMarkData.put(new Integer(0x60), new Short(FONT_F));
	    dungeonMarkData.put(new Integer(0x80), new Short(FONT_W));
	    dungeonMarkData.put(new Integer(0x81), new Short(FONT_R));
	    dungeonMarkData.put(new Integer(0x8E), new Short(FONT_P));
	    dungeonMarkData.put(new Integer(0x91), new Short(FONT_H));
	    dungeonMarkData.put(new Integer(0x92), new Short(FONT_A));
	    dungeonMarkData.put(new Integer(0x93), new Short(FONT_C));
	    dungeonMarkData.put(new Integer(0x94), new Short(FONT_P));
	    dungeonMarkData.put(new Integer(0xD0), null); // thru 0xDF
	    dungeonMarkData.put(new Integer(0xE0), new Short(FONT_S));
    }
  }

  public Short getDungeonTile(int dungeonData) {
    // Look up the map tile that the special dungeon value corresponds to.
    if (dungeonTileData != null)
	    return dungeonTileData.get(new Integer(dungeonData));
    return null;
  }

  public Short getDungeonMark(int dungeonData) {
    // Look up the map tile that the special dungeon value corresponds to.
    if (dungeonMarkData != null)
	    return dungeonMarkData.get(new Integer(dungeonData));
    return null;
  }
}
