<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<title>The Moongates Ultima IV Annex - Ultima IV Upgrade Patch</title>
</head>

<body bgcolor="#000000" text="#FFFFFF" link="#FFFF00" vlink="#FFFF00" alink="#FFFF00">

<div align="center"><center>

<table border="0" width="600" cellspacing="0" cellpadding="0" height="100">
  <tr>
    <td width="600" height="70" align="center" valign="bottom" background="/u4/images/Header.gif">
    <img src="../images/Gate.gif" width="50" height="50"> <font face="Arial"><img align="absMiddle" alt="The Moongates Ultima IV Annex" src="/u4/images/U4Header.gif" width="480" height="50"></font>
    <img src="../images/Gate.gif" width="50" height="50"></td>
  </tr>
  <tr>
    <td width="100%" height="34" valign="top" background="../Images/Menu.gif" align="center">
    <map name="FPMap0_I1">
    <area href="/" coords="10, 3, 80, 17" shape="rect">
    <area href="/u4" shape="rect" coords="85, 3, 142, 17">
    <area href="/u4/upgrade/Upgrade.htm" coords="147, 3, 200, 17" shape="rect">
    <area href="/u4/walkthrough/Walkthrough.htm" coords="206, 2, 283, 17"
    shape="rect">
    <area href="/u4/hints/Hints.htm" coords="288, 3, 323, 17" shape="rect">
    <area href="/u4/maps/Maps.htm" shape="rect" coords="329, 2, 364, 17">
    <area href="/u4/essays/Essays.htm" shape="rect" coords="369, 2, 414, 17">
    <area href="Tech.asp" shape="rect" coords="421, 2, 481, 16"></map><img border="0" src="images/U4Navtext.gif" usemap="#FPMap0_I1" width="490" height="18"></td>
  </tr>
</table>
</center></div>

<center>

<table border="0" width="600" cellspacing="0" cellpadding="0">
  <tr>
    <td background="Images/MG_Header.gif" width="600" height="50" colspan="3" valign="bottom" align="center"><font face="Times New Roman" color="#FFFFFF"><strong><big><big>Ultima
      IV Technical Info</big></big></strong></font></td>
  </tr>
  <tr>
    <td width="25" background="images/MG_Left.gif" align="right">&nbsp;</td>
</center>
    <td width="550" background="images/DarkTex3.gif">
        <table border="0" cellpadding="6" cellspacing="0" width="100%">
          <tr>
            <td valign="top">
      <p align="left"><b><font color="#FFFF00" face="Arial" size="2">Ultima IV:
      Behind The Scenes</font></b><font face="Comic Sans MS" size="2"><br>
      Would you like to know some of the workings behind this game? If so,
      you're in the right place. In creating this site as well as the graphic
      upgrade, I've had to uncover the details behind just about every file type
      used by the game program.</font></p>
      <p align="left"><font size="2" face="Arial"><b>Table of Contents</b></font></p>
      <ul>
        <li>
          <p align="left"><font size="2" face="Comic Sans MS"><a href="#g_orig">Graphics</a></font>
          <ul>
            <li>
              <p align="left"><font size="2" face="Comic Sans MS"><a href="#g_orig">Original
              Release</a> (16 colors)</font></li>
            <li>
              <p align="left"><font size="2" face="Comic Sans MS"><a href="#g_vga">VGA
              Patch Release</a> (256 colors)</font>
              <ul>
                <li>
                  <p align="left"><font face="Comic Sans MS" size="2"> <a href="images/misc/shapes.gif">View
                  or download the tile set</a></font></li>
                <li>
                  <p align="left"><font face="Comic Sans MS" size="2"><a
      href="images/misc/charset.gif">View or download the font</a> </font></li>
              </ul>
            </li>
            <li>
              <p align="left"><font size="2" face="Comic Sans MS">SVGA Patch
              Release (256 colors, 640x400 resolution) - SOON!</font></li>
          </ul>
        </li>
        <li>
          <p align="left"><font size="2" face="Comic Sans MS"><a href="#m_world">Maps</a></font>
          <ul>
            <li>
              <p align="left"><font size="2" face="Comic Sans MS"><a href="#m_world">World
              Map</a> (WORLD.MAP)</font></li>
            <li>
              <p align="left"><font size="2" face="Comic Sans MS"><a href="#m_town">Town
              Maps</a> (.ULT)</font></li>
            <li>
              <p align="left"><font size="2" face="Comic Sans MS"><a href="#m_con">Combat
              Maps</a> (.CON)</font></li>
            <li>
              <p align="left"><font size="2" face="Comic Sans MS"><a href="#m_dng">Dungeon
              Maps</a> (.DNG)</font></li>
          </ul>
        </li>
        <li>
          <p align="left"><font size="2" face="Comic Sans MS"><a href="#f_tlk">Dialog
          Files</a> (.TLK)</font></li>
        <li>
          <p align="left"><font size="2" face="Comic Sans MS"><a href="#f_sav">Savegame
          Files</a>  (.SAV)</font></li>
        <li>
          <p align="left"><font size="2" face="Comic Sans MS"><a href="#whatelse">What
          Else?</a></font></li>
        <li>
          <p align="left"><font size="2" face="Comic Sans MS"><a href="#tiles">Complete
          List of Tiles</a></font></li>
      </ul>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="g_orig"></a>The
      Original Graphics</b></font><br>
      <font face="Comic Sans MS" size="2">The following information refers to
      the original pre-patch data files, SHAPES.EGA, CHARSET.EGA, START.EGA,
      STONCRCL.EGA, KEY7.EGA, RUNE_<i>#</i>.EGA, and <i>virtuename</i>.EGA.</font></p>
      <p align="left"><b><font size="2" face="Arial">Evaluating the Color Data</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">EGA palette (<i>Netscape</i>
      might not show this table correctly. Too bad.):</font></p>
      <div align="center">
        <center>
        <table border="1" width="512" cellspacing="0" cellpadding="3" bgcolor="#000000">
          <tr>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">0</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">1</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">2</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">3</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">4</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">5</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">6</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">7</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">8</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">9</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">A</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">B</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">C</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">D</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">E</font></b></td>
            <td valign="middle" align="center" width="32" height="15"><b><font face="Arial" size="1">F</font></b></td>
          </tr>
          <tr>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#000000"><b><font face="Arial" size="1" color="#FFFFFF">0</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#000080"><b><font face="Arial" size="1" color="#FFFFFF">1</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#008000"><b><font face="Arial" size="1" color="#FFFFFF">2</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#008080"><b><font face="Arial" size="1" color="#FFFFFF">3</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#800000"><b><font face="Arial" size="1" color="#FFFFFF">4</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#800080"><b><font face="Arial" size="1" color="#FFFFFF">5</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#808000"><b><font face="Arial" size="1" color="#000000">6</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#C0C0C0"><b><font face="Arial" size="1" color="#000000">7</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#808080"><b><font face="Arial" size="1" color="#000000">8</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#0000FF"><b><font face="Arial" size="1" color="#000000">9</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#00FF00"><b><font face="Arial" size="1" color="#000000">10</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#00FFFF"><b><font face="Arial" size="1" color="#000000">11</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#FF4040"><b><font face="Arial" size="1" color="#000000">12</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#FF00FF"><b><font face="Arial" size="1" color="#000000">13</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#FFFF00"><b><font face="Arial" size="1" color="#000000">14</font></b></td>
            <td valign="middle" align="center" width="32" height="15" bgcolor="#FFFFFF"><b><font face="Arial" size="1" color="#000000">15</font></b></td>
          </tr>
        </table>
        </center>
      </div>
      <p align="left"><font face="Comic Sans MS" size="2">In the 16-color PC
      version of Ultima IV, each raw, unencoded byte holds two four bit color
      values. For example, a byte value of <font color="#00FFFF">255</font> is
      represented in binary as:</font></p>
      <p align="left"><font face="Comic Sans MS" size="2" color="#00FFFF">1 1 1
      1 1 1 1 1</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">In order to see which
      two colors will be assigned to the pixels, we break it into 4-bit halves
      like this:</font></p>
      <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">1
      1 1 1</font> <b><font color="#FF0000">|</font></b> <font color="#00FFFF">1
      1 1 1</font></font></p>
      <p align="left"><font face="Comic Sans MS" size="2">... and we see that
      these two binary halves are equivalent to the decimal numbers <font color="#00FFFF">15</font>
      and <font color="#00FFFF">15</font>. Therefore, a byte value of <font color="#00FFFF">255</font>
      means &quot;white&quot; followed by &quot;white&quot;.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">Similarly, if the byte
      value were <font color="#00FFFF">142</font>, then we would find it to mean
      &quot;dark gray&quot; followed by &quot;yellow&quot; like this:</font></p>
      <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">142</font>
      = <font color="#00FFFF">1 0 0 0 1 1 1 0</font><br>
      <font color="#00FFFF">1 0 0 0</font> <font color="#FF0000"><b>|</b></font>
      <font color="#00FFFF">1 1 1 0</font><br>
      <font color="#00FFFF">1 0 0 0</font> = <b><font color="#00FFFF">8</font></b><br>
      <font color="#00FFFF">1 1 1 0</font> = <b><font color="#00FFFF">14</font></b></font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The &quot;short
      cut&quot; is, of course, to just look at the hexadecimal value of the
      byte. If we know that the <font color="#00FFFF">142</font> mentioned above
      is <font color="#00FFFF">8E</font> in base 16, then all we have to is know
      that <font color="#00FFFF">8</font> is the first color and <font color="#00FFFF">E</font>
      (or <font color="#00FFFF">14</font> in base 10) is the second color.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">4F</font>
      means &quot;dark red&quot; followed by &quot;white&quot;.</font></p>
      <p align="left"><b><font size="2" face="Arial">SHAPES.EGA (tiles) and
      CHARSET.EGA (font)</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">SHAPES.EGA, the tile
      graphics set, is 32,768 bytes. There are 256 tiles, and each one is
      defined by 128 bytes of the file using the method described above. The
      data is read left to right and top to bottom. Each tile is 16x16 pixels.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">Thus, the top row (16
      pixels) of tile #0 is defined by the first 8 bytes of the file, which
      expands to color data for all 16 pixels. The next 8 bytes defines the
      second row of tile #0, and so forth.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">To find the offset of
      any particular tile, use: </font><font size="2" face="Courier New" color="#008080">offset
      = tile * 128</font><font face="Comic Sans MS" size="2"> where <font color="#008080">tile</font>
      is 0 to 255. To find the offset of a row within a tile, use: </font><font color="#008080" face="Courier New" size="2">offset
      = tile * 128 + y * 16</font><font face="Comic Sans MS" size="2"> where <font color="#008080">y</font>
      is 0 to 15.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">CHARSET.EGA is very
      similar to SHAPES.EGA. It defines the game font and can hold data for 16
      colors exactly as the tiles do (despite Origin's release using only the
      light gray color for text). The file is 8,192 bytes, the last 4,096 of
      which don't seem to be used at all. There are 128 characters in the font,
      each defined by 32 bytes. Each character is 8x8 pixels.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The top row (8 pixels)
      of character #0 is defined by the first 4 bytes of the file.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The formulas presented
      above can be easily modified to find a particular character or row.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2"><a href="images/misc/charset.gif">Click
      here</a> to see the ordering of the character set.</font></p>
      <p align="left"><b><font size="2" face="Arial">Remaining Applicable .EGA
      Files (run length encoded)</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">To start, the
      following table describes what these files contain:</font></p>
      <div align="left">
        <table border="0" cellspacing="3" cellpadding="3">
          <tr>
            <td><font face="Comic Sans MS" size="2">START.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the &quot;border&quot;
              graphic that divides up the main game screen</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">KEY7.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">graphic shown when you use
              your key of 3 parts</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">RUNE_0.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              valor</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">RUNE_1.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              honesty, justice, and honor</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">RUNE_2.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              compassion and sacrifice</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">RUNE_3.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              spirituality</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">RUNE_4.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              humility</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">RUNE_5.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">displayed after answering
              the final riddle</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">STONCRCL.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">displayed at congratulation
              screen</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">HONESTY.EGA</font></td>
            <td rowspan="11" valign="top"><font face="Comic Sans MS" size="2">the
              remaining files are all parts of the codex symbol that is built
              during the endgame. They are written to the screen with a logical
              OR, so the pixel positions of their values cannot be allow to
              overlap from file to file... among other things, our patch changed
              this. We also use 9 RUNE files instead of just 6. That way, we
              could have a unique vision for each virtue quest.</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">COMPASSN.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">VALOR.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">JUSTICE.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">SACRIFIC.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">HONOR.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">SPIRIT.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">HUMILITY.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">TRUTH.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">LOVE.EGA</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2">COURAGE.EGA</font></td>
          </tr>
        </table>
      </div>
      <p align="left"><font face="Comic Sans MS" size="2">All of these files use
      the same format, which differs from the tileset and font format. They are
      run length encoded (RLE), meaning that a signifier byte tells the program
      to start a &quot;run&quot; of one particular byte for a certain number of
      repetitions. For example, if you had a run of 180 &quot;black-black&quot;
      bytes (remember, each byte still holds information for two adjacent
      pixels), this would be compressed to just three bytes in the file: first
      the signifier, then the run length (up to 255), then the value to be
      repeated.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The signifier byte in
      ALL of these files is <font color="#00FFFF">02</font>.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">Therefore these 180
      bytes mentioned above would be compressed to: <font color="#00FFFF">02 B4
      00</font>. This would mean 360 pixels of black.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">For single values (not
      part of a run), the file simply includes the value. It is only when you
      want to have a run of 3 or more of the same value that it is economical to
      encode it as a run using the signifier byte.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The only strangeness
      occurs when you actually WANT to have a &quot;black-darkgreen&quot; byte
      (or bytes), which you should realize by now is ALSO the byte value <font color="#00FFFF">02</font>.
      Even if there is only one of these, it must be encoded as a run, like
      this: <font color="#00FFFF">02 01 02</font>.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The maximum run value
      is 255 (510 pixels in 16 color mode). If there are more than this a new run must be started.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">Because of the RLE,
      these files will all have varying lengths, and you must uncompress them to
      edit them. Once uncompressed, they will all, invariably, be <i>exactly
      32,000 bytes</i>.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">No matter how small
      the actual visible (non-black) part of the graphic, the entire 320x200
      screen is represented by these files. In other words, even if the
      meditation vision takes up a very small piece of screen, the image file
      itself will be 320x200 and mostly black. The game knows not to overwrite the
      screen with &quot;black-black&quot; bytes.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">A run does not stop at
      the end of a &quot;row&quot;. That is to say, if you started a run of
      black near the end of a row (say at coordinate x300,y0), it would
      &quot;wrap&quot; around to x0,y1 and continue on.</font></p>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="g_vga"></a>The New Graphics
      (VGA Patch)</b></font></p>
      <p align="left"><font face="Comic Sans MS" size="2">While I'm not even
      close to being able to explain Aradindae's programming genius in making it
      all work, I can tell you how the graphics files were changed to accommodate
      the 256 color palette.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FF00">Added</font>,
      <font color="#3366FF">Changed</font> , and <font color="#FF6666">Unused</font>
      files:</font></p>
      <div align="left">
        <table border="0" cellspacing="3" cellpadding="3">
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#00FF00">U4VGA.PAL</font></td>
            <td><font face="Comic Sans MS" size="2">768 byte color palette</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#3366FF">RUNE_0.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">displayed after answering
              the final riddle</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#3366FF">RUNE_1.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              honesty</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#3366FF">RUNE_2.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              compassion</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#3366FF">RUNE_3.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              valor</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#3366FF">RUNE_4.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              justice</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#3366FF">RUNE_5.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              sacrifice</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#00FF00">RUNE_6.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              honor</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#00FF00">RUNE_7.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              spirituality</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#00FF00">RUNE_8.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">the meditation vision for
              humility</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#00FF00">SHAPES.VGA</font></td>
            <td><font face="Comic Sans MS" size="2">the 256 color tile set</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#00FF00">CHARSET.VGA</font></td>
            <td><font face="Comic Sans MS" size="2">the 256 color character set</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#FF6666">SHAPES.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">old tiles</font></td>
          </tr>
          <tr>
            <td><font face="Comic Sans MS" size="2" color="#FF6666">CHARSET.EGA</font></td>
            <td><font face="Comic Sans MS" size="2">old font</font></td>
          </tr>
          <tr>
            <td colspan="2"><font face="Comic Sans MS" size="2">Other .EGA files
              are still used but converted to 256 color: START, STONCRCL, KEY7,
              and all virtues and principles (COMPASSN, LOVE, etc.)</font></td>
          </tr>
        </table>
      </div>
      <p align="left"><b><font size="2" face="Arial">The Palette File U4VGA.PAL</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">This file is 768 bytes
      and holds the color palette information used by the patched game. There
      are 256 possible colors (out of 262,144), and each one is described by 3
      bytes ranging in value from 0 to 63 (hex 00 to 3F). The byte order is Red,
      Green, Blue.</font></p>
      <p align="left"><b><font size="2" face="Arial">Evaluating the Color Data</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">This couldn't be
      simpler: 1 byte for 1 pixel. Breaking the information into two 4-bit pairs
      is no longer necessary as it was in the 16 color mode.</font></p>
      <p align="left"><b><font size="2" face="Arial">SHAPES.VGA (tiles) and
      CHARSET.VGA (font)</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">SHAPES.VGA, the tile
      graphics set, is 65,536 bytes. There are 256 tiles, and each one is
      defined by 256 bytes of the file. The data is read left to right and top
      to bottom. Each tile is 16x16 pixels.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">Thus, the top row (16
      pixels) of tile #0 is defined by the first 16 bytes of the file, with 1
      byte for 1 pixel. The next 16 bytes defines the second row of tile #0, and
      so forth.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">To find the offset of
      any particular tile, use: </font><font size="2" face="Courier New" color="#008080">offset
      = tile * 256</font><font face="Comic Sans MS" size="2"> where <font color="#008080">tile</font>
      is 0 to 255. To find the offset of a row within a tile, use: </font><font color="#008080" face="Courier New" size="2">offset
      = tile * 256 + y * 16</font><font face="Comic Sans MS" size="2"> where <font color="#008080">y</font>
      is 0 to 15. To find any particular pixel within a tile: </font><font color="#008080" face="Courier New" size="2">offset
      = tile * 256 + y * 16 + x</font><font face="Comic Sans MS" size="2"> where
      <font color="#008080">x</font> is also 0 to 15.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">CHARSET.VGA is very
      similar to SHAPES.VGA. It defines the game font and can hold data for 256
      colors exactly as the tiles do. The file is 8,192 bytes, all of which is
      now used. There are 128 characters in the font, each defined by 64 bytes.
      Each character is 8x8 pixels.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The top row (8 pixels)
      of character #0 is defined by the first 8 bytes of the file, and so on.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The formulas presented
      above can be easily modified to find a particular character, row, or pixel
      offset.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2"><a href="images/misc/charset.gif">Click
      here</a> to see the ordering of the character set.</font></p>
      <p align="left"><b><font size="2" face="Arial">Remaining .EGA Files (run
      length encoded)</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">All of the remaining
      files (indicated above) use the same format, which differs from the
      tileset and font format. They are run length encoded (RLE), meaning that a
      signifier byte tells the program to start a &quot;run&quot; of one
      particular byte for a certain number of repetitions. For example, if you
      had a run of 180 &quot;black&quot; pixels, this would be compressed to
      just three bytes in the file: first the signifier, then the run length (up
      to 255), then the color value to be repeated.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The signifier byte in
      ALL of these files is <font color="#00FFFF">02</font>.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">Therefore these 180
      bytes mentioned above would be compressed to: <font color="#00FFFF">02 B4
      00</font>. This would mean 180 pixels of black.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">For single values (not
      part of a run), the file simply includes the value. It is only when you
      want to have a run of 3 or more of the same value that it is economical to
      encode it as a run using the signifier byte <font color="#00FFFF">02</font>.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The only strangeness
      occurs when you actually WANT to have a &quot;darkgreen&quot; byte (or
      bytes). Even if there is only one of these, it must be encoded as a run,
      like this: <font color="#00FFFF">02 01 02</font>. For this reason, I
      duplicated the green color elsewhere in the palette file, and then wrote a
      little utility to find and replace any occurrence of <font color="#00FFFF">02</font>
      in the raw image data with its equivalent. This way, we get maximum
      efficiency from the RLE by sacrificing only one palette slot.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The maximum run length
      is 255 pixels. If there are more than this a new run must be started.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">Because of the RLE,
      these files will all have varying lengths, and you must uncompress them to
      edit them. Once uncompressed, they will all, invariably, be <i>exactly
      64,000 bytes</i>.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">No matter how small
      the actual visible (non-black) part of the graphic, the entire 320x200
      screen is represented by these files. In other words, even though the
      meditation visions takes up only the portion of the screen on which your
      party moves through the map, the image file itself will be 320x200 and
      mostly black. The game knows not to overwrite the screen with black.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">A run does not stop at
      the end of a &quot;row&quot;. That is to say, if you started a run of
      black near the end of a row (say at coordinate x300,y0), it would
      &quot;wrap&quot; around to x0,y1 and continue on.</font></p>
      <p align="left"><b><font size="2" face="Arial">Miscellaneous Changes</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">In order to make the
      endgame prettier, the logical OR operation used to write each piece of the
      codex symbol to screen was changed to an XOR operation. Therefore, it was
      necessary when <i>building</i> each successive image to XOR it with the
      previous image. It is not sufficient to simply draw and save each
      &quot;frame&quot; of the codex symbol... the colors will &quot;reverse
      out&quot; and look entirely unlike what you expected.</font></p>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="m_world"></a>The World
      Map (WORLD.MAP)</b></font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The WORLD.MAP is a
      65,536 byte file, whose values correspond to the <a href="#tiles">tile
      list</a> shown at the bottom of this page.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">You might expect this
      file to be a simple matrix of 256x256 tiles, which corresponds to the
      world size. You would be wrong about that. Presumably to save memory, the
      Ultima IV program will only load a small section of the map at a time, and
      in order to facilitate this the world map is divided into smaller pieces.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">It is in fact divided
      into a total of 64 32x32 chunks, with the chunks arrayed as follows:</font></p>
      <div align="center">
        <center>
        <table border="1" cellspacing="0" width="256" cellpadding="0" background="images/misc/worldmapg.gif" height="256">
          <tr>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">1</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">2</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">3</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">4</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">5</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">6</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">7</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">8</font></td>
          </tr>
          <tr>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">9</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">10</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">11</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">12</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">13</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">14</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">15</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">16</font></td>
          </tr>
          <tr>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">17</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">18</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">19</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">20</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">21</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">22</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">23</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">24</font></td>
          </tr>
          <tr>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">25</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">26</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">27</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">28</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">29</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">30</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">31</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">32</font></td>
          </tr>
          <tr>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">33</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">34</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">35</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">36</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">37</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">38</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">39</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">40</font></td>
          </tr>
          <tr>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">41</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">42</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">43</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">44</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">45</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">46</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">47</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">48</font></td>
          </tr>
          <tr>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">49</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">50</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">51</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">52</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">53</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">54</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">55</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">56</font></td>
          </tr>
          <tr>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">57</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">58</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">59</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">60</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">61</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">62</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">63</font></td>
            <td valign="middle" align="center" width="32" height="32"><font face="Comic Sans MS" size="1" color="#FFFF00">64</font></td>
          </tr>
        </table>
        </center>
      </div>
      <p align="left"><font face="Comic Sans MS" size="2">As far as actual byte
      order in the file, chunk 1 will take up the first 1,024 bytes. Chunk 2 the
      second 1,024, and so forth.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">Within these chunks,
      the byte order is as expected. That is, the first 32 bytes are row 1, the
      second 32 bytes are row 2, etc.</font></p>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="m_town"></a>The Town
      Maps (.ULT)</b></font></p>
      <p align="left"><font face="Comic Sans MS" size="2">These files are the
      town maps. They also contain information about the people in the towns and
      their starting positions.</font></p>
      <p align="left"><b><font face="Arial" size="2">Standard ULT File
      Structure (1,280 bytes):</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Map (1,024 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">A 32x32 matrix
        defining the tiles that make up the town. Values taken from
        the <a href="#tiles">tile list</a> below. Only the physical structure of
        the town is defined here. Characters and shopkeepers are NOT!</font></p>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Population (256 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">Series of bytes
        defining the appearance, starting position, behavior, and dialog tree
        for up to 32 characters.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2">Each series of 32
        bytes within this 256 byte block represents a different type of data,
        but they always come in the same <i>character</i> order. In other words,
        population offset 0, 32, 64, 96, 128, 160, 192, and 224 all refer to the
        same NPC, as do offsets 1, 33, 65, 97, 129, 161, 193, and 225.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2">If less than 32 NPC
        characters exist in the town, then zeroes come FIRST in the sequence.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> =
        tile to represent character ??? (guard, child, merchant, etc.)<br>
        Changing this value seems to have no effect. In order to alter the
        graphic, you must change the similar value below. This value may have
        something to do with the combat prowess of the NPC, but that is
        unconfirmed and seems unlikely. Always, this number will be very close
        or identical to the other similar value explained below. Let me know if
        you are sure of the meaning of this byte.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> = X
        position of character (0 to 31)</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> = Y
        position of character (0 to 31)</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> =
        tile to represent character (guard, child, merchant, etc.)<br>
        In some cases, this is a repeat of the first number. Other times it is 1
        or 3 less depending on if it is part of a 2-frame or 4-frame animation.<br>
        If the NPC's graphic is part of an animated frame series, this number
        should always be the first number in that series. Changing this number
        will alter the graphic used to represent the NPC.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> = X
        position of character (repeat of previous X value)</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> = Y
        position of character (repeat of previous Y value)</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> =
        character behavior:</font></p>
        <blockquote>
          <div align="left">
            <table border="1" cellpadding="4" cellspacing="0">
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">0</font></td>
                <td><font face="Comic Sans MS" size="2">Stationary</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">1</font></td>
                <td><font face="Comic Sans MS" size="2">Normal Wandering</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">128</font></td>
                <td><font face="Comic Sans MS" size="2">Follows PC closely (or
                  is Merchant)</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">255</font></td>
                <td><font face="Comic Sans MS" size="2">Tracks Player &amp;
                  Attacks (as a monster does)</font></td>
              </tr>
            </table>
          </div>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> =
        character identity (indexed in corresponding TLK file)<br>
        Value is 1 to 16 (as ordered in the town's TLK)<br>
        or 0 (no identity -- a silent character or a merchant has no TLK id)</font></p>
      </blockquote>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="m_con"></a>The
      Combat Maps (.CON)</b></font></p>
      <p align="left"><font face="Comic Sans MS" size="2">These files are the
      combat screens. For example, if you attack the shore from your ship, melee
      combat will be carried out on the SHIPSHOR.CON map.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2">The exception is
      SHRINE.CON, which DOES NOT have the same structure. Even if you melee over
      a shrine tile, you will see grasslands. The SHRINE.CON is instead used
      only when you meditate and has a different byte structure.</font></p>
      <p align="left"><b><font face="Arial" size="2">Standard CON File
      Structure (192 bytes):</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Header (64 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">Defines starting
        positions of player's party members and monsters.<br>
        Possible X and Y values range from 0 to 10 (A).</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>byte 0-15</i> =
        starting X position of monsters (up to 16 monsters)<br>
        <i>byte 16-31</i> = Y position of monsters<br>
        <i>0</i> and <i>15</i> create a coordinate pair, as do <i>1</i> and <i>16</i>,
        etc. If fewer than 16 monsters, monster starting positions are randomly
        chosen from among these coordinate pairs.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>byte 32-39</i> =
        X position of party members (up to 8 party members)<br>
        <i>byte 40-47</i>= Y position of party members<br>
        <i>32</i> and <i>40</i> create a coordinate pair, as do <i>33</i> and <i>41</i>,
        etc. Positions are not random; they are always used in order no matter
        how many characters in your party.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>byte 48-63</i> = <font color="#00FFFF">08
        AD 83 C0 AD 83 C0 AD 83 C0 A0 00 B9 A6 08 F0</font><br>
        Purpose unknown. Seems to be constant among all files.</font></p>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Body (121 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">An 11x11 matrix
        defining the tiles on which combat will take place. Values taken from
        the <a href="#tiles">tile list</a> below.</font></p>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Footer (7 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">Always seems to be <font color="#00FFFF">8D
        00 00 00 00 47 09</font>. Purpose unknown.</font></p>
      </blockquote>
      <p align="left"><b><font face="Arial" size="2">SHRINE.CON File
      Structure (192 bytes):</font></b></p>
      <blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Body (121 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">An 11x11 matrix
        defining the tiles on which the characters meditate. Values taken from
        the <a href="#tiles">tile list</a> below. The Party Icon (1F) is
        explicitly defined in this matrix.</font></p>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Footer (71 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"> Purpose unknown. No
        similar files exist for comparison.</font></p>
      </blockquote>
      </blockquote>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="m_dng"></a>The Dungeon
      Files (.DNG)</b></font></p>
      <p align="left"><font face="Comic Sans MS" size="2">These files are the 3D
      dungeon maps. They also contain information about all the top-down view
      rooms that you might enter, the creatures that inhabit them, any floor
      triggers present, and starting positions for characters and monsters.
      Please note that ABYSS.DNG is slightly different and will be explained
      separately.</font></p>
      <p align="left"><b><font face="Arial" size="2">Standard DNG File
      Structure (4,608 bytes):</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2"><i>3D Map (512 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">Each dungeon level
        is an 8x8 grid, and there are 8 levels per dungeon.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2">Thus the first 64
        bytes are Level 1, and the first 8 bytes are &quot;row&quot; 1 of that
        level. And so on through 512 bytes.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2">The choice of values
        here is limited (values shown in hex):</font></p>
        <blockquote>
          <div align="left">
            <table border="1" cellpadding="4" cellspacing="0">
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">00</font></td>
                <td><font face="Comic Sans MS" size="2">Nothing / Corridor</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">10</font></td>
                <td><font face="Comic Sans MS" size="2">Ladder Up</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">20</font></td>
                <td><font face="Comic Sans MS" size="2">Ladder Down</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">30</font></td>
                <td><font face="Comic Sans MS" size="2">Ladder Up &amp; Down</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">40</font></td>
                <td><font face="Comic Sans MS" size="2">Treasure Chest</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">50</font></td>
                <td><font face="Comic Sans MS" size="2">Ceiling Hole <i>(not
                  used?)</i></font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">60</font></td>
                <td><font face="Comic Sans MS" size="2">Floor Hole <i>(not
                  used?)</i></font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">70</font></td>
                <td><font face="Comic Sans MS" size="2">Magic Orb</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">80</font></td>
                <td><font face="Comic Sans MS" size="2">Winds / Darkness Trap</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">81</font></td>
                <td><font face="Comic Sans MS" size="2">Falling Rock Trap</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">8E</font></td>
                <td><font face="Comic Sans MS" size="2">Pit Trap</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">90</font></td>
                <td><font face="Comic Sans MS" size="2">Plain Fountain</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">91</font></td>
                <td><font face="Comic Sans MS" size="2">Healing Fountain</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">92</font></td>
                <td><font face="Comic Sans MS" size="2">Acid Fountain</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">93</font></td>
                <td><font face="Comic Sans MS" size="2">Cure Fountain</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">94</font></td>
                <td><font face="Comic Sans MS" size="2">Poison Fountain</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">A0</font></td>
                <td><font face="Comic Sans MS" size="2">Poison Field</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">A1</font></td>
                <td><font face="Comic Sans MS" size="2">Energy Field</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">A2</font></td>
                <td><font face="Comic Sans MS" size="2">Fire Field</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">A3</font></td>
                <td><font face="Comic Sans MS" size="2">Sleep Field</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">B0</font></td>
                <td><font face="Comic Sans MS" size="2">Altar</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">C0</font></td>
                <td><font face="Comic Sans MS" size="2">Door</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">D0-DF</font></td>
                <td><font face="Comic Sans MS" size="2">Dungeon Room - in order
                  from later in the file</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">E0</font></td>
                <td><font face="Comic Sans MS" size="2">Secret Door</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">F0</font></td>
                <td><font face="Comic Sans MS" size="2">Wall</font></td>
              </tr>
            </table>
          </div>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">Ladders DO line up
        between levels, and the hallways wrap around left to right and top to
        bottom (like Pac-Man's escape tunnels!)</font></p>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Dungeon Rooms
      (4,096 bytes, 16 rooms of 256 bytes each):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">There are 16 rooms
        per normal DNG file. The order in which these rooms appear in the file
        determines what identifier is used to place it on the 3D dungeon map.
        (See above.) The first room is D0, the second is D1... and the 16th is
        DF. Each room is defined as follows and totals <i>256 bytes</i>:</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>16 bytes</i> =
        floor triggers; changes that occur when you step on certain coordinates
        in the room. There can be four such triggers per room, and each one is
        defined by four bytes. If there are less than four triggers, 0's
        (meaning no trigger) come LAST in the sequence.</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">trigger byte 1 =
          tile you wish to PLACE (see tile list)</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">trigger byte 2 =
          where you step to activate the trigger. Examples:</font></p>
          <blockquote>
            <p align="left"><font face="Comic Sans MS" size="2">Hex value 85
            means you step on coordinate x8,y5 to activate the trigger.</font></p>
            <p align="left"><font face="Comic Sans MS" size="2">Hex value A3
            means you step on coordinate x10,y3 to activate the trigger.</font></p>
          </blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">trigger byte 3 =
          coordinate of first tile to change.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">trigger byte 4 =
          coordinate of second tile to change.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">TRIGGER EXAMPLE:<font color="#00FFFF"><br>
          46 85 75 65</font> - Places a Fire Field at (7, 5) and (6, 5) when you
          step on (8, 5).</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">PRACTICAL TRIGGER
          EXAMPLE: (Abyss Level 7 room)<br>
          <font color="#00FFFF">16 44 06 05</font><font color="#000080"> | </font><font color="#00FFFF">16
          44 04 40 </font><font color="#000080">| </font><font color="#00FFFF">16
          44 50 60</font><font color="#000080"> | </font><font color="#00FFFF">16
          88 51 15</font></font></p>
          <div align="center">
            <center>
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td valign="middle" align="center" width="16" height="16"></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">0</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">1</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">2</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">3</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">4</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">5</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">6</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">7</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">8</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">9</font></td>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">A</font></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">0</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">1</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/220.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">2</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/188.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/156.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">3</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/188.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/228.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/196.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">4</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/220.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/156.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/196.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/184.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">5</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">6</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">7</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">8</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/secret.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">9</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
              </tr>
              <tr>
                <td valign="middle" align="center" width="16" height="16"><font size="1" face="Arial">A</font></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/22.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/127.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
                <td valign="middle" align="center" width="16" height="16"><img border="0" src="tilescripts/126.gif"></td>
              </tr>
            </table>
            </center>
          </div>
          <p align="left"><font face="Comic Sans MS" size="2">Stepping on the
          secret door at (8, 8) replaces the walls next to each wisp (5, 1 and
          1, 5) with tile floors so you can reach the monsters. <font color="#FF0000">[Trigger
          #4]</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Stepping on the
          spot occupied by the gazer (4, 4) creates purple tile floor exits to
          the north (4, 0; 5, 0; 6, 0) and west (0, 4; 0, 5; 0, 6). Notice that
          stepping on a single coordinate (4, 4) can activate multiple triggers.
          <font color="#FF0000">[Trigger #1, #2, &amp; #3]</font></font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>16 bytes</i> =
        master (first frame) creature tile, as seen in tile list.<br>
        Defines types of monsters that will be in the room. If there are less
        than 16 creatures, 0's (meaning no monster) come FIRST in the sequence.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>16 bytes</i> =
        Creature starting X positions (0 to 10 (A)).</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>16 bytes</i> =
        Creature starting Y positions (0 to 10 (A)).</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>8 bytes</i> =
        Party starting X positions (0 to 10 (A)) if entering from top.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>8 bytes</i> =
        Party starting Y positions (0 to 10 (A)) if entering from top.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>8 bytes</i> =
        Party starting X positions (0 to 10 (A)) if entering from right.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>8 bytes</i> =
        Party starting Y positions (0 to 10 (A)) if entering from right.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>8 bytes</i> =
        Party starting X positions (0 to 10 (A)) if entering from bottom.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>8 bytes</i> =
        Party starting Y positions (0 to 10 (A)) if entering from bottom.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>8 bytes</i> =
        Party starting X positions (0 to 10 (A)) if entering from left.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>8 bytes</i> =
        Party starting Y positions (0 to 10 (A)) if entering from left.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>121 bytes</i> =
        11x11 matrix defining the physical structure of the room.<br>
        Creatures are NOT included here; only walls, floors, rocks, fields, etc.</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>7 bytes</i> = <font color="#00FFFF">00
        00 00 00 00 00 00</font> (filler)</font></p>
      </blockquote>
      <p align="left"><b><font size="2" face="Arial">ABYSS.DNG File
      Structure (16,896 bytes):</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2"><i>3D Map (512 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">The Abyss is exactly
        the same as the other dungeons in this regard (8x8 levels, 8 levels).</font></p>
        <p align="left"><font face="Comic Sans MS" size="2">However, the Abyss
        has 64 dungeon rooms instead of just 16. The dungeon rooms are still
        specified with D0-DF, but with a twist:</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Level 1 &amp;
          Level 2 share a single D# series. (8 rooms each)</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Level 3 &amp;
          Level 4 share a single D# series. (8 rooms each)</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Level 5 &amp;
          Level 6 share a single D# series. (Level 5 has 5 rooms, Level 6 has 11
          rooms)</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Level 7 &amp;
          Level 8 share a single D# series. (Level 7 has 4 rooms, Level 8 has 12
          rooms)</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">The D# still refers
        to the order in which the rooms are defined in the file; it simply
        resets itself every two levels.</font></p>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Dungeon Rooms
      (16,384 bytes, 64 rooms of 256 bytes each):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">There are 64 rooms
        defined in the ABYSS.DNG file. The byte structure is exactly the same as
        the other dungeon files. There are simply more rooms tacked on to the
        end of the file.</font></p>
      </blockquote>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="f_tlk"></a>The
      Dialog Files (.TLK)</b></font></p>
      <p align="left"><font face="Comic Sans MS" size="2">These define the NPC
      character names, behaviors, and conversation trees (well, conversation <i>twigs</i>
      in the case of this game). The presentation order of the characters (from
      1 to 16) determines their &quot;ID number&quot;, which is used in the ULT
      town maps. (See Above.)</font></p>
      <p align="left"><b><font face="Arial" size="2">Standard TLK File
      Structure (4,608 bytes):</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Single Character
      (288 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>byte 0</i> = Yes
        / No branch trigger. This value determines which keyword will cause the
        NPC to ask you a Yes or No question after his response.</font></p>
        <blockquote>
          <div align="left">
            <table border="1" cellpadding="4" cellspacing="0">
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">0</font></td>
                <td><font face="Comic Sans MS" size="2">Character has no query.</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">3</font></td>
                <td><font face="Comic Sans MS" size="2">JOB</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">4</font></td>
                <td><font face="Comic Sans MS" size="2">HEALTH</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">5</font></td>
                <td><font face="Comic Sans MS" size="2">KEYWORD 1</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">6</font></td>
                <td><font face="Comic Sans MS" size="2">KEYWORD 2</font></td>
              </tr>
            </table>
          </div>
          <p align="left"><font face="Comic Sans MS" size="2">It is possible
          that <font color="#00FFFF">1</font> and <font color="#00FFFF">2</font>
          values would correspond to NAME and LOOK, but this does not exist in
          the game and has not been tested.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>byte 1</i> = Yes
        / No response affects Humility?</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">1</font>
          =The response affects Humility.<br>
          Answering YES to the NPC's question will cause a loss of 5 Humility
          points. Answering NO will gain 5 Humility points.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">0</font>
          = The response does not affect humility.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>byte 2</i> =
        Turns away?</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">This is the
          possibility of the NPC turning away from you during a conversation. As
          best as I can determine, it is something like a percent chance.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Example: Brigant
          the Troll in Bucaneer's Den has a turn away value of <font color="#00FFFF">128
          </font>(<font color="#00FFFF">80 hex</font>), and he turns away about
          50% of the time. (This is the highest one in the game.) He also will
          attack you sometimes, but that's probably just because he's a
          troll!&nbsp; Cricket the Bard in Britain has a turn away value of <font color="#00FFFF">32
          </font>(<font color="#00FFFF">20 hex</font>), so in my estimation he
          will turn away about 13% of the time because he's busy playing the
          lute.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>byte 3-287</i> =
        The rest of it. Each piece of information is separated by a value <font color="#00FFFF">0</font>
        byte. Line feeds are explicitly stated with a value <font color="#00FFFF">0A</font>
        (hex) byte. Leftover space at the end of the record is padded with value
        <font color="#00FFFF">0</font> bytes. If a piece of information does not
        need to exist for this particular character, it is simply replaced by a
        capital &quot;A&quot; (<font color="#00FFFF">41 hex</font>). If
        eliminating a KEYWORD in this manner, the KEYWORD should be <font color="#00FFFF">41
        20 20 20</font> (an &quot;A&quot; with 3 spaces).</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Order of
          Information:</font></p>
          <ol>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">Name</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">Pronoun (He,
              She, It)</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">LOOK
              description</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">JOB response</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">HEALTH
              response</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">KEYWORD 1
              response</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">KEYWORD 2
              response</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">Yes / No
              Question</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">YES response</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">NO response</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">KEYWORD 1 (4
              characters, all capital letters)</font></li>
            <li>
              <p align="left"><font face="Comic Sans MS" size="2">KEYWORD 2 (4
              characters, all capital letters)</font></li>
          </ol>
        </blockquote>
      </blockquote>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="f_sav"></a>The Saved
      Game Files (PARTY.SAV, OUTMONST.SAV, MONSTERS.SAV)</b></font>
      </p>
      <p align="left"><b><font face="Arial" size="2">PARTY.SAV File
      Structure (502 bytes):</font></b></p>
      <p align="left"><font face="Comic Sans MS" size="2">This file holds all
      information about your characters, inventory, and current game.</font></p>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Header (8 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>4 bytes</i> =
        header, purpose unknown</font></p>
        <p align="left"><font face="Comic Sans MS" size="2"><i>4 bytes</i> = #
        of moves, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Example: <font color="#00FFFF">EA
          0C 02 00</font> = $00020CEA = 134,378 moves</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Maximum presumably
          4,294,967,295 moves.</font></p>
        </blockquote>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Character Data x8 (39
      bytes each character, total 312 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        current hitpoints, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Maximum should be
          800 hp, or <font color="#00FFFF">20 03</font>.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>= max
        hitpoints, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Maximum should be
          800 hp, or <font color="#00FFFF">20 03</font>.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        experience, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Maximum should be
          9999 xp, or <font color="#00FFFF">0F 27</font>.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        strength, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Maximum should be
          50, or <font color="#00FFFF">32 00</font>.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        dexterity, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Maximum should be
          50, or <font color="#00FFFF">32 00</font>.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        intelligence, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Maximum should be
          50, or <font color="#00FFFF">32 00</font>.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        mana, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Maximums should be
          determined by class as follows:</font></p>
          <div align="left">
            <table border="1" cellpadding="4" cellspacing="0">
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS">Class</font></td>
                <td><font face="Comic Sans MS" size="2">Formula</font></td>
                <td><font face="Comic Sans MS" size="2">max DEC</font></td>
                <td><font face="Comic Sans MS" size="2">max HEX</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2">Mage</font></td>
                <td><font size="2" face="Comic Sans MS">2 * INT</font></td>
                <td><font face="Comic Sans MS" size="2">99 <font color="#FF0000">*</font></font></td>
                <td><font face="Comic Sans MS" size="2"><font color="#00FFFF">63
                  00</font> <font color="#FF0000">*</font></font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS">Bard</font></td>
                <td><font size="2" face="Comic Sans MS">INT</font></td>
                <td><font face="Comic Sans MS" size="2">50</font></td>
                <td><font face="Comic Sans MS" size="2" color="#00FFFF">32 00</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS">Fighter</font></td>
                <td><font size="2" face="Comic Sans MS">0</font></td>
                <td><font face="Comic Sans MS" size="2">0</font></td>
                <td><font face="Comic Sans MS" size="2" color="#00FFFF">00 00</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS">Druid</font></td>
                <td><font size="2" face="Comic Sans MS">3/2 * INT</font></td>
                <td><font face="Comic Sans MS" size="2">75</font></td>
                <td><font face="Comic Sans MS" size="2" color="#00FFFF">4B 00</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS">Tinker</font></td>
                <td><font size="2" face="Comic Sans MS">1/2 * INT</font></td>
                <td><font face="Comic Sans MS" size="2">25</font></td>
                <td><font face="Comic Sans MS" size="2" color="#00FFFF">19 00</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS">Paladin</font></td>
                <td><font size="2" face="Comic Sans MS">INT</font></td>
                <td><font size="2" face="Comic Sans MS">50</font></td>
                <td><font size="2" face="Comic Sans MS" color="#00FFFF">32 00</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS">Ranger</font></td>
                <td><font size="2" face="Comic Sans MS">INT</font></td>
                <td><font size="2" face="Comic Sans MS">50</font></td>
                <td><font size="2" face="Comic Sans MS" color="#00FFFF">32 00</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS">Shepherd</font></td>
                <td><font size="2" face="Comic Sans MS">0</font></td>
                <td><font size="2" face="Comic Sans MS">0</font></td>
                <td><font size="2" face="Comic Sans MS" color="#00FFFF">00 00</font></td>
              </tr>
            </table>
          </div>
          <p align="left"><font face="Comic Sans MS" size="2"><font color="#FF0000">*</font>
          while the Mage formula would dictate a maximum of 100 mana, the actual
          maximum is 99; it is likely that this was implemented for display
          purposes. 49 INT will yield 98 MP, but 50 INT will yield only 99 MP.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        unknown, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">The purpose of
          these bytes is unknown.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        weapon carried, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Value is <font color="#00FFFF">00
          00</font> to <font color="#00FFFF">0F 00</font>:</font></p>
          <div align="left">
            <table border="1" cellpadding="4" cellspacing="0">
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">00
                  00</font></td>
                <td><font size="2" face="Comic Sans MS">Hands (no weapon)</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS" color="#00FFFF">01
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Staff</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">02
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Dagger</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS" color="#00FFFF">03
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Sling</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS" color="#00FFFF">04
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Mace</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS" color="#00FFFF">05
                  00</font></td>
                <td><font size="2" face="Comic Sans MS">Axe</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS" color="#00FFFF">06
                  00</font></td>
                <td><font size="2" face="Comic Sans MS">Sword</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS" color="#00FFFF">07
                  00</font></td>
                <td><font size="2" face="Comic Sans MS">Bow</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS" color="#00FFFF">08
                  00</font></td>
                <td><font size="2" face="Comic Sans MS">Crossbow</font></td>
              </tr>
              <tr>
                <td align="right"><font size="2" face="Comic Sans MS" color="#00FFFF">09
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Oil</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">0A
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Halberd</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">0B
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Magic Axe</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">0C
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Magic Sword</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">0D
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Magic Bow</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">0E
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Magic Wand</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">0F
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Mystic Sword</font></td>
              </tr>
            </table>
          </div>
        <p align="left"><font face="Comic Sans MS" size="2">If you're into
        cheating a little, weapons can be assigned with an editor that would
        normally be forbidden to a character's class. An armada of Magic Wands
        works quite nicely.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        armor worn, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Value is <font color="#00FFFF">00
          00</font> to <font color="#00FFFF">07 00</font>:</font></p>
          <div align="left">
            <table border="1" cellpadding="4" cellspacing="0">
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">00
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Skin (no armor)</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">01
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Cloth</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">02
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Leather</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">03
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Chain</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">04
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Plate</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">05
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Magic Chain</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">06
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Magic Plate</font></td>
              </tr>
              <tr>
                <td align="right"><font face="Comic Sans MS" size="2" color="#00FFFF">07
                  00</font></td>
                <td><font face="Comic Sans MS" size="2">Mystic Armor</font></td>
              </tr>
            </table>
          </div>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>16 bytes </i>=
        name</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">MAY be 15
          characters and a terminator of <font color="#00FFFF">00</font>.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">The practical
          LIMIT, however, is 8 characters and a <font color="#00FFFF">00</font>
          terminator. <i>Any name longer than 8 characters will corrupt the
          display.</i></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Example:<i>
          Geoffrey</i></font></p>
          <p align="left"><font face="Comic Sans MS" size="2" color="#00FFFF">47
          65 6F 66 66 72 65 79 00</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Any values
          following the <font color="#00FFFF">00</font> byte but within this 16
          byte block may be considered &quot;junk&quot; data.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> =
        gender</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">0B
          </font>= Male<br>
          <font color="#00FFFF">0C </font>= Female</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> =
        class</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">00</font>
          = Mage<br>
          <font color="#00FFFF">01</font> = Bard<br>
          <font color="#00FFFF">02</font> = Fighter<br>
          <font color="#00FFFF">03</font> = Druid<br>
          <font color="#00FFFF">04</font> = Tinker<br>
          <font color="#00FFFF">05</font> = Paladin<br>
          <font color="#00FFFF">06</font> = Ranger<br>
          <font color="#00FFFF">07</font> = Shepherd</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> =
        status (ASCII capital G, P, S, or D)</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">G = <font color="#00FFFF">47</font>
          = Good<br>
          P = <font color="#00FFFF">50</font> = Poisoned<br>
          S = <font color="#00FFFF">53</font> = Sleeping<br>
          D = <font color="#00FFFF">44</font> = Dead</font></p>
        </blockquote>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2"><i>Footer (182 bytes):</i></font></p>
      <blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>4 bytes</i> =
        food, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 999,999
          or <font color="#00FFFF">3F 42 0F 00</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">For display
          purposes, the last 2 digits of the decimal food total are truncated on
          screen. Each &quot;move&quot; subtracts an actual amount of food equal
          to the number of characters in your party.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">When buying food,
          each pack of &quot;25 rations&quot; is actually equal to 2,500 food in
          the save file.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes</i> =
        gold, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 9,999
          gold or <font color="#00FFFF">0F 27</font></font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>16 bytes</i> =
        virtue levels, reversed storage order each 2 bytes</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">2 bytes for each
          of 8 virtues, determines your virtuousness or partial avatarhood in
          that virtue. Value may be 0 to 99.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">00
          00</font> = partial Avatar (partial ankh appears between Food and Gold
          totals on screen)<br>
          <font color="#00FFFF">01 00</font> through <font color="#00FFFF">63 00</font>
          = virtue level (99 is better than 0).</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">At 99 (<font color="#00FFFF">63
          00</font>) Hawkwind the Seer would tell you to seek the elevation in
          that virtue.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Virtue order is
          standard: Honesty, Compassion, Valor, Justice, Sacrifice, Honor,
          Spirituality, Humility</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes</i> =
        torches, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 99, <font color="#00FFFF">63
          00</font></font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes</i> =
        gems, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 99, <font color="#00FFFF">63
          00</font></font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes</i> =
        keys, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 99, <font color="#00FFFF">63
          00</font></font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes</i> =
        sextants, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 99, <font color="#00FFFF">63
          00</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">But 01 00 will
          suffice, since sextants do not disappear with use.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>16 bytes</i> =
        reserve armor, reversed storage order each 2 bytes</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 99, <font color="#00FFFF">63
          00</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">8 types of Armor
          ordered as in the armor table shown in the above section on character
          data.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Note that you may
          specify &quot;reserve&quot; Hands with an editor but this has no
          effect on the game. A quantity will never show next to Hands, nor can
          you sell Hands at a shop.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>32 bytes</i> =
        reserve weapons, reversed storage order each 2 bytes</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 99, <font color="#00FFFF">63
          00</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">16 types of
          Weapons ordered as in the weapon table shown in the above section on
          character data.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Note that you may
          specify &quot;reserve&quot; Skin (No Armour) with an editor but this
          has no effect on the game. A quantity will never show next to No
          Armour, nor can you sell Skin at a shop.&nbsp; =)</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>16 bytes</i> =
        reagents, reversed storage order each 2 bytes</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 99, <font color="#00FFFF">63
          00</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">8 types of
          Reagents, ordered:<br>
          Ash, Ginseng, Garlic, Silk<br>
          Bloodmoss, Black Pearl, Nightshade, Mandrake</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>52 bytes</i> =
        spells, reversed storage order each 2 bytes</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">maximum = 99, <font color="#00FFFF">63
          00</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Quantity of mixed
          Spells are saved in alphabetical order from A-'Awaken' to Z-'Z-Down'.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> =
        quest item storage #1</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">This byte stores
          which quest ITEMS you currently possess or which quests have been
          completed.<br>
          Each bit of the byte represents a different item:</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Binary order: 7 6
          5 4 3 2 1 0</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">0 = Skull of
          Mondain (picked up from ocean)<br>
          1 = Skull of Mondain (thrown into Abyss)<br>
          2 = Candle of Love<br>
          3 = Book of Truth<br>
          4 = Bell of Courage<br>
          5 = Key part: T<br>
          6 = Key part: L<br>
          7 = Key part: C</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Note that bit 0
          and 1 should not both be set to ON...<br>
          <i>Scenario:</i><br>
          1. You retrieve the Skull from the deep sea by searching with your
          boat = Bit 0 set to ON. The Skull is now in inventory.<br>
          2. You use the Skull at the entrance of the Abyss, destroying it = Bit
          0 set to OFF, Bit 1 set to ON. The Skull is removed from inventory,
          but the game knows NOT to let you pick it up again from the ocean.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> =
        quest item storage #2</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">This byte stores
          which quest ITEMS you currently possess or which quests you have
          completed.<br>
          Each bit of the byte represents a different item:</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Binary order: 7 6
          5 4 3 2 1 0</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">0 = Silver Horn<br>
          1 = Wheel of The HMS Cape<br>
          2 = Candle has been Used at Abyss<br>
          3 = Book has been Used at Abyss<br>
          4 = Bell has been Used at Abyss<br>
          5 = <i>unused</i><br>
          6 = <i>unused</i><br>
          7 = <i>unused</i></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Note: to freely
          enter the Abyss, you must have Used the Skull, Bell, Book, and Candle.
          All the proper bit flags must be set, as described.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> =
        longitude location (X coordinate location)</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Corresponds to the
          two parts of the second given sextant coordinate (Longitude).</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">A' through P' = <font color="#00FFFF">0</font>
          through <font color="#00FFFF">F<br>
          </font>A&quot; through P&quot; = <font color="#00FFFF">0</font>
          through <font color="#00FFFF">F</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Example: <font color="#00FFFF">E8</font>
          = Longitude: O'I&quot;</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">May also be though
          of as the absolute X coordinate on the 256x256 world map matrix.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> =
        latitude location (Y coordinate location)</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">Corresponds to the
          two parts of the first given sextant coordinate (Latitude).</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">A' through P' = <font color="#00FFFF">0</font>
          through <font color="#00FFFF">F<br>
          </font>A&quot; through P&quot; = <font color="#00FFFF">0</font>
          through <font color="#00FFFF">F</font></font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Example: <font color="#00FFFF">E9</font>
          = Latitude: O'J&quot;</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">May also be though
          of as the absolute Y coordinate on the 256x256 world map matrix.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> =
        Virtue Stone storage</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">This byte stores
          which colored Virtue Stones you currently possess.<br>
          Each bit of the byte represents a different stone:</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Binary order: 7 6
          5 4 3 2 1 0</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">0 = Blue<br>
          1 = Yellow<br>
          2 = Red<br>
          3 = Green<br>
          4 = Orange<br>
          5 = Purple<br>
          6 = White<br>
          7 = Black</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>1 byte</i> = Rune
        storage</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">This byte stores
          which Runes you currently possess.<br>
          Each bit of the byte represents a different Rune:</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Binary order: 7 6
          5 4 3 2 1 0</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">0 = Honesty<br>
          1 = Compassion<br>
          2 = Valor<br>
          3 = Justice<br>
          4 = Sacrifice<br>
          5 = Honor<br>
          6 = Spirituality<br>
          7 = Humility</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        number of Characters in Party, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">01
          00</font> = you are alone<br>
          <font color="#00FFFF">08 00</font> = party is full (necessary to
          complete the game)<br>
          <font color="#00FFFF">00 00</font> = &quot;No Party Formed!&quot;
          error at start up.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Example: 6
          characters in party means that the first six character records (as
          described above) represent the characters in the party.</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes </i>=
        Vehicle / Party Icon, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2">This represents
          the party's current mode of transport, as defined in the <a href="#tiles">tile
          list</a> below.</font></p>
          <p align="left"><font face="Comic Sans MS" size="2">Examples:<br>
          <font color="#00FFFF">12 00</font> = you are in a boat facing east<br>
          <font color="#00FFFF">14 00</font> = you are on a horse facing west<br>
          <font color="#00FFFF">18 00</font> = you are aboard the hot air
          balloon<br>
          <font color="#00FFFF">1F 00</font> = you are on foot</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2"><i>2 bytes</i> =
        Balloon status, reversed storage order</font></p>
        <blockquote>
          <p align="left"><font face="Comic Sans MS" size="2"><font color="#00FFFF">00
          00</font> = on ground, or not aboard balloon<br>
          <font color="#00FFFF">01 00</font> = balloon is flying</font></p>
        </blockquote>
        <p align="left"><font face="Comic Sans MS" size="2">TO BE CONTINUED...</font></p>
      </blockquote>
      <p align="left"><font face="Comic Sans MS" size="2">OUTMONST.SAV &amp;
      MONSTERS.SAV Info forthcoming...</font>
      </p>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="whatelse"></a>What
      Didn't I Figure Out?</b></font>
      </p>
      <p align="left"><font face="Comic Sans MS" size="2">I covered a great deal
      of information about the program, but there is much I did not bother to
      work out.</font>
      </p>
      <p align="left"><font face="Comic Sans MS" size="2">I used to get a lot of
      questions regarding the character creation sequence, most of which were
      something like &quot;Why didn't you update the title screen, the gypsy
      carnival, or the abacus?&quot;</font>
      </p>
      <p align="left"><font face="Comic Sans MS" size="2">The answer is that I
      simply couldn't figure out how these files worked. I spent probably a
      total of 6 hours looking at them, trying to make sense of the scheme,
      until I decided that it is such a minor part of the game it wasn't worth
      additional time. Believe me, I would have loved to redo to carnival graphics and
      &quot;tarot&quot; cards, but in my opinion, it's really something you
      spend a very small amount of time looking at compared to the rest of the
      game.</font>
      </p>
      <p align="left"><font face="Arial" size="2" color="#FFFF00"><b><a name="tiles"></a>The
      Complete List of Tiles</b></font><br>
      <font face="Comic Sans MS" size="2"><table border="0" width="520" align="center" cellpadding="2" cellspacing="8">
<tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">0</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">0</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/0s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Deep Water</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">1</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">1</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/1s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Medium Water</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">2</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">2</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/2s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Shallow Water</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">3</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">3</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/3.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Swamp</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">4</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">4</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/4.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Grasslands</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">5</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">5</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/5.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Scrubland</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">6</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">6</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/6.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Forest</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">7</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">7</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/7.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Hills</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">8</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">8</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/8.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mountains</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">9</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">9</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/9.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Dungeon Entrance</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">10</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/10s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Town</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">11</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/11s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Castle</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">12</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/12.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Village</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">13</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/13.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lord British's Castle, West Wing</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">14</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/14s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lord British's Castle, Entrance</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">15</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/15.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lord British's Castle, East Wing</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">16</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">10</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/16s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ship</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">17</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">11</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/17.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ship</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">18</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">12</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/18s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ship</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">19</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">13</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/19.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ship</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">20</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">14</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/20.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Horse</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">21</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">15</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/21.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Horse</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">22</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">16</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/22.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Tile Floor</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">23</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">17</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/23.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bridge</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">24</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">18</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/24.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Balloon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">25</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">19</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/25.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bridge</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">26</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">1A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/26.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bridge</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">27</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">1B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/27.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ladder Up</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">28</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">1C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/28.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ladder Down</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">29</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">1D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/29.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ruins</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">30</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">1E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/30.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Shrine</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">31</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">1F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/31.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Questing Party</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">32</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">20</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/32s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mage</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">33</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">21</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/33.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mage</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">34</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">22</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/34s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bard</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">35</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">23</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/35.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bard</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">36</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">24</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/36s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Fighter</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">37</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">25</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/37.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Fighter</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">38</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">26</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/38s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Druid</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">39</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">27</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/39.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Druid</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">40</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">28</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/40s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Tinker</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">41</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">29</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/41.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Tinker</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">42</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">2A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/42s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Paladin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">43</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">2B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/43.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Paladin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">44</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">2C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/44s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ranger</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">45</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">2D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/45.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ranger</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">46</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">2E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/46s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Shepherd</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">47</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">2F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/47.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Shepherd</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">48</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">30</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/48.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Column</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">49</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">31</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/49.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">White SW</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">50</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">32</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/50.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">White SE</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">51</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">33</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/51.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">White NW</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">52</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">34</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/52.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">White NE</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">53</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">35</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/53.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mast</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">54</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">36</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/54.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ship's Wheel</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">55</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">37</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/55.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rocks</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">56</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">38</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/56.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lying Down</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">57</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">39</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/57.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Stone Wall</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">58</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">3A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/58.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Locked Door</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">59</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">3B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/59.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Unlocked Door</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">60</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">3C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/60.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Chest</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">61</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">3D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/61.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ankh</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">62</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">3E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/62.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Brick Floor</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">63</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">3F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/63.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Wooden Planks</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">64</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">40</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/64.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Moongate, Transient</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">65</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">41</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/65.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Moongate, Transient</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">66</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">42</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/66.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Moongate, Transient</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">67</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">43</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/67s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Moongate, Blue</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">68</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">44</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/68s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Poison Field</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">69</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">45</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/69s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Energy Field</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">70</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">46</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/70s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Fire Field</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">71</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">47</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/71s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Sleep Field</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">72</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">48</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/72.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Solid Barrier</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">73</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">49</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/73.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Hidden Passage</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">74</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">4A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/74.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Altar</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">75</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">4B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/75s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Spit</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">76</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">4C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/76s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lava Flow</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">77</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">4D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/77.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Missile</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">78</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">4E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/78.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Magic Sphere</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">79</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">4F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/79.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Attack Flash</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">80</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">50</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/80.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Guard</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">81</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">51</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/81.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Guard</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">82</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">52</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/82s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Citizen</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">83</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">53</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/83.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Citizen</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">84</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">54</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/84s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bard, Singing</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">85</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">55</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/85.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bard, Singing</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">86</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">56</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/86s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Jester</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">87</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">57</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/87.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Jester</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">88</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">58</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/88s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Unfortunate</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">89</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">59</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/89.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Unfortunate</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">90</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">5A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/90s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Child</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">91</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">5B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/91.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Child</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">92</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">5C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/92s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bull</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">93</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">5D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/93.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bull</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">94</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">5E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/94s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lord British</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">95</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">5F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/95.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lord British</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">96</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">60</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/96.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">A</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">97</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">61</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/97.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">B</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">98</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">62</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/98.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">C</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">99</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">63</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/99.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">D</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">100</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">64</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/100.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">E</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">101</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">65</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/101.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">F</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">102</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">66</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/102.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">G</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">103</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">67</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/103.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">H</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">104</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">68</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/104.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">I</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">105</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">69</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/105.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">J</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">106</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">6A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/106.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">K</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">107</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">6B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/107.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">L</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">108</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">6C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/108.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">M</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">109</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">6D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/109.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">N</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">110</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">6E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/110.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">O</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">111</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">6F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/111.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">P</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">112</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">70</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/112.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Q</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">113</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">71</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/113.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">R</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">114</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">72</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/114.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">S</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">115</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">73</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/115.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">T</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">116</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">74</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/116.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">U</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">117</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">75</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/117.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">V</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">118</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">76</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/118.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">W</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">119</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">77</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/119.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">X</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">120</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">78</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/120.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Y</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">121</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">79</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/121.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Z</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">122</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">7A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/122.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Space</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">123</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">7B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/123.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Right</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">124</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">7C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/124.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Left</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">125</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">7D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/125.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Window</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">126</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">7E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/126.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Blank</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">127</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">7F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/127.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Brick Wall</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">128</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">80</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/128s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Pirate Ship</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">129</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">81</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/129.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Pirate Ship</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">130</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">82</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/130s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Pirate Ship</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">131</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">83</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/131.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Pirate Ship</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">132</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">84</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/132s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Nixie</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">133</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">85</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/133.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Nixie</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">134</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">86</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/134s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Giant Squid</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">135</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">87</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/135.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Giant Squid</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">136</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">88</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/136s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Sea Serpent</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">137</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">89</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/137.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Sea Serpent</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">138</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">8A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/138s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Seahorse</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">139</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">8B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/139.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Seahorse</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">140</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">8C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/140s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Whirlpool</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">141</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">8D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/141.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Whirlpool</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">142</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">8E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/142s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Storm</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">143</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">8F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/143.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Storm</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">144</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">90</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/144s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rat</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">145</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">91</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/145.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rat</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">146</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">92</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/146.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rat</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">147</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">93</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/147.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rat</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">148</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">94</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/148s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bat</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">149</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">95</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/149.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bat</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">150</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">96</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/150.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bat</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">151</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">97</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/151.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Bat</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">152</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">98</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/152s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Giant Spider</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">153</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">99</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/153.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Giant Spider</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">154</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">9A</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/154.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Giant Spider</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">155</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">9B</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/155.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Giant Spider</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">156</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">9C</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/156s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ghost</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">157</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">9D</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/157.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ghost</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">158</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">9E</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/158.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ghost</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">159</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">9F</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/159.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ghost</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">160</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A0</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/160s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Slime</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">161</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A1</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/161.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Slime</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">162</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A2</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/162.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Slime</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">163</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A3</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/163.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Slime</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">164</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A4</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/164s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Troll</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">165</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A5</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/165.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Troll</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">166</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A6</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/166.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Troll</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">167</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A7</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/167.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Troll</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">168</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A8</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/168s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Gremlin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">169</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">A9</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/169.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Gremlin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">170</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">AA</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/170.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Gremlin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">171</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">AB</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/171.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Gremlin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">172</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">AC</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/172s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mimic</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">173</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">AD</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/173.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mimic</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">174</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">AE</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/174.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mimic</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">175</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">AF</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/175.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mimic</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">176</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B0</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/176s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Reaper</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">177</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B1</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/177.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Reaper</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">178</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B2</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/178.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Reaper</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">179</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B3</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/179.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Reaper</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">180</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B4</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/180s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Insect Swarm</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">181</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B5</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/181.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Insect Swarm</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">182</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B6</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/182.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Insect Swarm</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">183</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B7</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/183.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Insect Swarm</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">184</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B8</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/184s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Gazer</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">185</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">B9</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/185.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Gazer</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">186</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">BA</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/186.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Gazer</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">187</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">BB</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/187.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Gazer</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">188</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">BC</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/188s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Phantom</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">189</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">BD</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/189.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Phantom</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">190</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">BE</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/190.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Phantom</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">191</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">BF</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/191.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Phantom</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">192</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C0</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/192s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Orc</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">193</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C1</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/193.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Orc</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">194</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C2</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/194.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Orc</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">195</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C3</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/195.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Orc</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">196</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C4</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/196s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Skeleton</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">197</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C5</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/197.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Skeleton</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">198</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C6</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/198.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Skeleton</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">199</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C7</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/199.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Skeleton</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">200</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C8</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/200s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rogue</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">201</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">C9</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/201.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rogue</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">202</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">CA</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/202.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rogue</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">203</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">CB</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/203.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Rogue</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">204</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">CC</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/204s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Python</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">205</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">CD</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/205.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Python</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">206</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">CE</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/206.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Python</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">207</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">CF</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/207.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Python</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">208</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D0</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/208s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ettin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">209</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D1</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/209.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ettin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">210</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D2</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/210.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ettin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">211</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D3</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/211.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Ettin</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">212</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D4</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/212s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Headless</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">213</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D5</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/213.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Headless</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">214</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D6</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/214.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Headless</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">215</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D7</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/215.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Headless</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">216</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D8</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/216s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Cyclops</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">217</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">D9</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/217.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Cyclops</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">218</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">DA</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/218.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Cyclops</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">219</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">DB</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/219.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Cyclops</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">220</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">DC</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/220s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Wisp</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">221</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">DD</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/221.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Wisp</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">222</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">DE</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/222.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Wisp</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">223</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">DF</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/223.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Wisp</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">224</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E0</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/224s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mage</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">225</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E1</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/225.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mage</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">226</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E2</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/226.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mage</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">227</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E3</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/227.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Mage</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">228</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E4</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/228s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lich</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">229</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E5</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/229.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lich</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">230</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E6</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/230.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lich</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">231</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E7</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/231.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lich</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">232</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E8</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/232s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lava Lizard</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">233</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">E9</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/233.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lava Lizard</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">234</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">EA</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/234.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lava Lizard</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">235</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">EB</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/235.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Lava Lizard</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">236</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">EC</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/236s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Zorn</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">237</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">ED</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/237.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Zorn</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">238</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">EE</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/238.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Zorn</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">239</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">EF</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/239.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Zorn</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">240</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F0</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/240s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Daemon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">241</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F1</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/241.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Daemon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">242</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F2</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/242.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Daemon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">243</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F3</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/243.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Daemon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">244</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F4</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/244s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Hydra</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">245</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F5</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/245.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Hydra</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">246</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F6</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/246.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Hydra</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">247</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F7</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/247.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Hydra</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">248</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F8</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/248s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Dragon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">249</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">F9</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/249.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Dragon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">250</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">FA</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/250.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Dragon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">251</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">FB</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/251.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Dragon</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">252</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">FC</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/252s.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Balron</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">253</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">FD</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/253.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Balron</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">254</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">FE</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/254.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Balron</td></tr><tr><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">255</td><td valign="middle" align="right" width="20"><font face="Comic Sans MS" size="2">FF</td><td width="32" bgcolor="#FFFF00"><img src="tilescripts/255.gif" width="32" height="32"></td><td valign="middle" width="448"><font face="Comic Sans MS" size="2">Balron</td></tr>
      </table>
      </font>
            </td>
          </tr>
          <center>

        <center>
        </table>
        </center>
      </center>
    </td>
    <td width="25" background="images/MG_Right.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="images/MG_B.gif" width="600" height="25" colspan="3">&nbsp;</td>
  </tr>
</table>

<div align="center"><center>
<table border="0" width="600" cellspacing="0" cellpadding="0">
  <tr>
    <td width="600" align="center"><font face="Arial" size="1">Area maintained by <a
      href="mailto:jcsteele@moongates.com">Joshua C. Steele</a>. All original
    material may not be copied or mirrored without permission.<br>
    This site is not affiliated with Origin Systems. Please Read the <a
      href="/Disclaimer.htm">Disclaimer</a>.</font></td>
  </tr>
</table>
</center></div>


</body>
</html>
