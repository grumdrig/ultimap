import java.io.*;
import java.awt.image.BufferedImage;


class U5Parser extends UltimaParser {

  protected String getDataDir() { return "ULTIMAV/"; }

  private static final String tileDataFilename = "./u5tiles.16"; // from TILES.16
  protected String getTileFilename() { return "u5tiles.png"; }
  private static final String tileCGADataFilename = "u5tiles.4"; // from TILES.4
  private static final String tileCGAFilename = "u5cga.png";

  private static final String fontDataFilename = "u5text.16"; // from TEXT.16
  protected String getFontFilename() { return "u5font.png"; }
  private static final String fontCGADataFilename = "u5text.4"; // from TEXT.4
  private static final String fontCGAFilename = "u5cgaf.png";

  private static final String britanniaFilename = "BRIT";
  private static final String underworldFilename = "UNDER";
  private static final String worldDataFormat = "%s.DAT";
  private static final String worldExpandedFilename = "./u5fullbrit.dat";
  private static final String worldStitchedFormat = "u5%s.png";
  //private static final String worldChunkFormat = "u5%s%02o.png";
  private static final String worldChunkFormat = "u5%s%d.png";
  private static final String worldMinimapFormat = "u5%sminimap.png";

  private static final String towneDataExtension = ".DAT";
  private static final String towneFormat = "u5map%s.png";

  private static final String miscMapsDataFilename = "MISCMAPS.DAT";
  private static final String miscMapsFormat = "u5miscmaps%d.png";
  
  private static final String textDataExtension = ".DAT";
  private static final String textFormat = "u5txt%S.txt";
  /*
  private static final String conflictDataExtension = ".CON";
  private static final String conflictFormat = "u4con%S.png";
  private static final String conflictSpawnFormat = "u4con%Ss.png";
  */
  private static final String dungeonDataFilename = "DUNGEON.DAT";
  private static final String dungeonFormat = "u5dng%s%d.png";
  //private static final String dungeonRoomFormat = "u5dng%Sr%02d.png";
  //private static final String dungeonRoomSpawnFormat = "u5dng%Sr%02ds.png";
  //private static final String dungeonRoomTriggerFormat = "u5dng%Sr%02dt.png";


  public U5Parser() {
    CHUNK_SIZE = 16;
  }

  /* Notes on other .16 / .4 files, as they appear upon cursory,
     post-u6decode inspection...  All of these are educated guesses.
     None of this has been properly decoded, just hack & glance.

     ULTIMA.# - opening animation "Ultima V / Warriors of Destiny" ?
     STORY#.# - illustrations for opening story
     STARTSC.# - ? presumably opening screen, 
     MON#.# - ? #3 looks watery, #2 looks more gray, #4 is greenish.. who knows
     ITEMS.# - ? think i see some fountains and such, not sure
     DNG1.# - the "rough hewn cavern" dungeon POV art
     DNG2.# - the "mine" dungeon POV art
     DNG3.# - the "nice proper gray dungeon" dungeon POV art
     CREATE.# - looks like blue, and flames ... more intro stuff, I reckon
     ENDSC.# - lots of white.  big coin symbol or something, I'd guess
     END#.# - reminiscent of STORY# - probably illustrations of ending story
   */


	/*
    BRIT.CBT and DUNGEON.CBT data are in rows of 32 bytes -- the
    first 11 bytes per row are part of a battleground/room;
    presumably the rest of the data are for placement etc (triggers too?)

  private static void parseConflict(final String fileName) {
    // There is a CAMP.DNG but it ain't no dungeon as far as I can tell.
    if (fileName.equals("CAMP")) {
      System.out.println("Whatever CAMP is, it's not a dungeon file I can process.");
      System.exit(0);
    }

    // Get source bitmap
    BufferedImage tiles = getTileImages();

    // First 64 bytes of data are header (unless this is a shrine file)
    int headerSize = 64;
    if (fileName.equals("SHRINE"))
      headerSize = 0;

    // Get data
    final String dataFileName = fileName + conflictDataExtension;
    byte[] data = new byte[headerSize + 121];
    // (expect that there are exactly 7 more bytes which are always a certain value)
    getData(data, getInputStream(dataFileName), true);

    // Prepare the image we will draw to - always 11x11 tiles
		final int FIELD_SIZE = 11;
    BufferedImage image = new BufferedImage(FIELD_SIZE * TILE_SIZE,
																						FIELD_SIZE * TILE_SIZE,
																						BufferedImage.TYPE_INT_RGB);
    parseTiles(tiles, image, data, headerSize, FIELD_SIZE);

    int i = 0;
    int tileX = 0, tileY = 0;
    int mapX = 0, mapY = 0;

    // Save image file
    String outFileName = String.format(conflictFormat, fileName);
    outFileName = outFileName.toLowerCase();
    writeImage(image, outFileName);

    // Now go back to header and make another image with the spawn data
    int x = 0, y = 0;
    if (headerSize > 0) {
      // Get font images
      BufferedImage font = getFontImages();

      U4Data parseData = new U4Data();

      // Monster spawn data
      tileX = (parseData.TILE_DOT & 0x0F) * TILE_SIZE;
      tileY = (parseData.TILE_DOT >> 4) * TILE_SIZE;
      for (i = 0; i < TILE_SIZE; ++i) {
        x = data[i];
        y = data[i + TILE_SIZE];
        mapX = x * TILE_SIZE;
        mapY = y * TILE_SIZE;
        copyMaskedTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);
      }

      // PC spawn data - 8 party slots
      tileX = (parseData.TILE_BOX & 0x0F) * TILE_SIZE;
      tileY = (parseData.TILE_BOX >> 4) * TILE_SIZE;
      int fontX = 0;
      int fontY = (parseData.FONT_1 >> 4) * FONT_SIZE;
      for (i = 0; i < 8; ++i) {
        x = data[i+32];
        y = data[i+40];
        mapX = x * TILE_SIZE;
        mapY = y * TILE_SIZE;
        copyMaskedTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);

        fontX = (((parseData.FONT_1 & 0x0F) + i) * FONT_SIZE);
        // (index # looks slightly better just right of center, IMO)
        copyMaskedTile(font, image, fontX, fontY, mapX + 5, mapY + 4, FONT_SIZE);
      }
	    
      // (there are also 16 more bytes which expect to have exact value)

      // Save image file
      outFileName = String.format(conflictSpawnFormat, fileName);
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
    }
  }
  */
  private void parseDungeon() {
    // Get source images
    BufferedImage tiles = getTileImages();
    //BufferedImage font = getFontImages();
	
    // Get data - in U5 all maps are packed into one file
    //** room data, if it exists, is another question (.CBT?)
    byte[] data = new byte[4096];
    getData(data, getInputStream(dungeonDataFilename), true);

    // There are special codes in dungeon maps that go back to map tiles
    final int dungeonRoom = 0xF0; // detect F0-FF w/ fewer entries
    //** there's also 3 or 4 different signs, so could do something similar

    // There are eight floors to each dungeon, and eight dungeons
    int i = 0;
    int datum = 0;
    short tile = 0;
    int tileX = 0, tileY = 0;
    int mapX = 0, mapY = 0;
    final int DUNGEONS = 8;
		final int FLOORS = 8; // 8 floors per dungeon
		final int FLOOR_SIZE = 8; // and each floor is 8x8
		final int ROOMS = 16; // 16 rooms per standard dungeon
    for (int dungeon = 0; dungeon < DUNGEONS; ++dungeon) {
      int dungeonOffset = dungeon * FLOORS * FLOOR_SIZE * FLOOR_SIZE;
      String dungeonName = U5Data.getDungeonName(dungeon);
      for (int level = 0; level < FLOORS; ++level) {
        // Prepare the image we will draw floor to - always 8x8 tiles
        BufferedImage image = new BufferedImage(FLOOR_SIZE * TILE_SIZE,
                                                FLOOR_SIZE * TILE_SIZE,
                                                BufferedImage.TYPE_INT_RGB);
        int floorOffset = level * FLOOR_SIZE * FLOOR_SIZE;
        for (i = 0; i < FLOOR_SIZE * FLOOR_SIZE; ++i) {
          datum = unsign(data[dungeonOffset + floorOffset + i]);

          // Turn the special dungeon code into the regular tile map code
          tile = U5Data.getDungeonTile(datum);
          if (tile < 0) {
            // This had better be a dungeon room! (16 codes for those)
            if ((datum > 0xF0) && (datum - 0xF0 < ROOMS)) {
              tile = U5Data.getDungeonTile(dungeonRoom);
            } 
            else {
              System.err.println(String.format("Unexpected dungeon map datum %X on level %d", datum, level + 1));
              System.exit(1);
            }
          }
          else if (tile == U5Data.TILE_HEX) {
            // good? bad? treat walls similarly?
            tile = U5Data.getDungeonFloorTile(dungeon);
          }

          // Put the tile on the map
          tileX = (tile & 0x0F) * TILE_SIZE;
          tileY = (tile >> 4) * TILE_SIZE;
          mapX = (i % FLOOR_SIZE) * TILE_SIZE;
          mapY = (i / FLOOR_SIZE) * TILE_SIZE;
          copyTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);
          /*
          // If there's a letter that should be drawn on top, do that
          tile = parseData.getDungeonMark(datum);
          if (tile == null) {
            // Handling the dungeon rooms special - check if we've got one
            int diff = (datum - 0xD0) + 1;
            if ((datum >= 0xD0) && (diff <= ROOMS)) {
              // Write as two-digit number for simplicity
              tileX = (parseData.FONT_0 & 0x0F) * FONT_SIZE;
              tileY = (parseData.FONT_0 >> 4) * FONT_SIZE;
              mapY += FONT_SIZE >> 1;
              copyMaskedTile(font,
                             image,
                             tileX + (diff / 10) * FONT_SIZE,
                             tileY,
                             mapX,
                             mapY,
                             FONT_SIZE);
              copyMaskedTile(font,
                             image,
                             tileX + (diff % 10) * FONT_SIZE,
                             tileY,
                             mapX + FONT_SIZE,
                             mapY,
                             FONT_SIZE);
            }
          } else {
            tileX = (tile.shortValue() & 0x0F) * FONT_SIZE;
            tileY = (tile.shortValue() >> 4) * FONT_SIZE;
            mapX += FONT_SIZE >> 1;
            mapY += FONT_SIZE >> 1;
            copyMaskedTile(font, image, tileX, tileY, mapX, mapY, FONT_SIZE);
          }
          */
        }

        // Save image file
        String outFileName = String.format(dungeonFormat, dungeonName, level + 1);
        outFileName = outFileName.toLowerCase();
        writeImage(image, outFileName);
        System.out.print(".");
      }
    }
    /*
    // In drawing rooms, will often draw some boxes, so let's just cook that up once.
    final int boxX = (parseData.TILE_BOX & 0x0F) * TILE_SIZE;
    final int boxY = (parseData.TILE_BOX >> 4) * TILE_SIZE;

    // There are either 16 or 64 dungeon rooms following.  Just trust data length.
    // (also expect 7 padding bytes at the end, all = 0)
    int j = 0, k = 0;
    int roomNum = 1;
		final int ROOM_SIZE = 11;
    for (i = FLOORS * (FLOOR_SIZE * FLOOR_SIZE);
				 i < data.length - 7;
				 i += 256, roomNum++) {
      // Prepare the image we will draw to - always 11x11 tiles
      BufferedImage image = new BufferedImage(ROOM_SIZE * TILE_SIZE,
																							ROOM_SIZE * TILE_SIZE,
																							BufferedImage.TYPE_INT_RGB);

      // First skip ahead to the floorplan - standard tile data
      int skip = 16 + (16 + 16 + 16) + (8 + 8) + (8 + 8) + (8 + 8) + (8 + 8);
      parseTiles(tiles, image, data, i + skip, 11);

      // Save image file - the empty room/floor plan
      String outFileName = String.format(dungeonRoomFormat, fileName, roomNum);
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
      System.out.print(".");

      // If there is any trigger data, make an image to contain that.
      // But oftentimes there are no triggers, so skip this if you find none
      skip = 0;

      // Word has it that if there are any triggers, they come first in the list.
      // There has to be an elegant way of doing this, but for now...
      if( unsign(data[skip + i + 0]) != 0 ||
          unsign(data[skip + i + 1]) != 0 ||
          unsign(data[skip + i + 2]) != 0 ||
          unsign(data[skip + i + 3]) != 0 ) {
        // At least one trigger exists.
        int triggerX = 0, triggerY = 0;
		
        // Create a separate image for the trigger map (will reuse orig for spawn map)
        BufferedImage triggerImage = new BufferedImage(image.getWidth(),
																											 image.getHeight(),
																											 image.getType());
        copyTile(image, triggerImage, 0, 0, 0, 0, ROOM_SIZE * TILE_SIZE);
	    
        for (j = 0; j < 16; j += 4) {
          // The trigger changes tiles to another kind
          datum = unsign(data[(skip + i) + j]);
          tileY = (datum >> 4) * TILE_SIZE;
          tileX = (datum & 0x0F) * TILE_SIZE;

          // The trigger is located somewhere on map
          datum = unsign(data[(skip + i) + j + 1]);
          // Only here did it occur to Origin to pack 11x11 coords into 1 byte.
          triggerX = (datum >> 4) * TILE_SIZE;
          triggerY = (datum & 0x0F) * TILE_SIZE;

          //** Some triggers reverse what other triggers do.
          // That gets overwritten here, and whatever trigger is listed last wins
          // the battle for visibility.  Pink boxes will mitigate that effect.

          if (triggerX != 0 || triggerY != 0 || tileX != 0 || tileY != 0) {
            // or, theoretically, one of the following bytes, but I'll doom the
            // case where the NW corner of screen creates deep ocean for the moment
		    
            // The trigger itself
            copyMaskedTile(tiles,
													 triggerImage,
													 (parseData.TILE_DOT & 0x0F) * TILE_SIZE,
													 (parseData.TILE_DOT >> 4) * TILE_SIZE,
													 triggerX,
													 triggerY,
													 TILE_SIZE);

            // The affected tile(s).  Sometimes only one of the fields seems
            // to be used, so I am taking a 0,0 value to mean no change.
            // (Seems they might have chosen to count from 1 for this reason...)
            for (k = 2; k <= 3; k++) {
              datum = unsign(data[(skip + i) + j + k]);
              mapX = (datum >> 4) * TILE_SIZE;
              mapY = (datum & 0x0F) * TILE_SIZE;
              if (mapX != 0 || mapY != 0) {
                copyTile(tiles, triggerImage, tileX, tileY, mapX, mapY, TILE_SIZE);
                copyMaskedTile(tiles, triggerImage, boxX, boxY, mapX, mapY, TILE_SIZE);
                // Maybe a nice little dotted line? overkill I reckon
              }
            }
          }
        }

        // Save trigger file
        outFileName = String.format(dungeonRoomTriggerFormat, fileName, roomNum);
        outFileName = outFileName.toLowerCase();
        writeImage(triggerImage, outFileName);
        System.out.print(".");
      }

      // Now image can get the monster/player spawn data.
      // Starting with monsters:
      skip = 16;
      for (j = 0; j < 16; ++j) {
        datum = unsign(data[i + j + skip]);
        // Empty monster slots appear first (and aren't deep ocean)
        if (datum != 0) {
          tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
          tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
          mapX = (data[i + j + skip + 16]) * TILE_SIZE;
          mapY = (data[i + j + skip + 32]) * TILE_SIZE;
          copyTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);

          // I had thought there was a chance that some monsters overlapped.
          // Then I wrote a bunch of code to indicate those cases,
          // and then I decided I was wrong.  Now there is this comment.
        }
      }

      // And finishing up with the nice guys (8 party slots):
      skip = 16 + (16 + 16 + 16);
      tileX = (parseData.FONT_1 & 0x0F) * FONT_SIZE;
      tileY = (parseData.FONT_1 >> 4) * FONT_SIZE;
      for (j = 0; j < 8; ++j) {
        // There are 4 entry points for each character, for each entrance
        for (k = 0; k < 4; ++k) {
          mapX = (data[(skip + i) + j + (k * 16)]) * TILE_SIZE;
          mapY = (data[(skip + i) + (j + 8) + (k * 16)]) * TILE_SIZE;

          // When there's no expectation they'll come in some way, data's 0,0.
          // I'll trust that no individual is ever expected to enter from there.
          if (mapX != 0 || mapY != 0) {
            copyMaskedTile(tiles, image, boxX, boxY, mapX, mapY, TILE_SIZE);
            // (index # looks slightly better just right of center, IMO)
            copyMaskedTile(font, image, tileX, tileY, mapX+5, mapY+4, FONT_SIZE);
          }
        }
        tileX += FONT_SIZE;
      }

      // Save spawn file
      outFileName = String.format(dungeonRoomSpawnFormat, fileName, roomNum);
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
      System.out.print(".");
    }
    */
    System.out.println();
  }

  private void expandWorldMap() throws IOException {
    // The original BRIT.DAT skips chunks which are 100% deep ocean (01).
    // To spare the world parser the pain of dealing with this, we can
    // create an expanded .DAT file which can then be treated like normal.
    // Following is the world map - we must insert our chunks in place of Os:
    /*
           OOXXXXXXXXXXXXOX    00 01 0E
           OXXXXXXXXXXXXXXO    10 1F
           XXXXXXXXXXXXXXXO    2F
           XXXXXXXXXXXXXXXX
           XXXXXXXXXXXXXXXX
           XXXXXXXXXXXXXXXX
           XXXXXXXXXXXOXXXX    6B
           XXXXXXXOOOXXOXXX    77 78 79 7C
           OXXXXXXOOOXXOXXX    80 87 88 89 8C
           XXXXXXXOXOOOOXXO    97 99 9A 9B 9C 9F
           XXXXXXXOXOOXXOOO    A7 A9 AA AD AE AF
           OOXXXXXXOOOOOOXX    B0 B1 B8 B9 BA BB BC BD
           OXXOXXXXOOXXOXXX    C0 C3 C8 C9 CC
           OXXOXXXXOOXXOXXX    D0 D3 D8 D9 DC
           OXXXXXXXXXXXXXXX    E0
           OOOXXOXXXXXXXXXX    F0 F1 F2 F5
    */
    final short[] insertChunks = {
      0x00, 0x01, 0x0E, 0x10, 0x1F, 0x2F, 0x6B, 0x77, 0x78, 0x79, 0x7C,
      0x80, 0x87, 0x88, 0x89, 0x8C, 0x97, 0x99, 0x9A, 0x9B, 0x9C, 0x9F,
      0xA7, 0xA9, 0xAA, 0xAD, 0xAE, 0xAF, 0xB0, 0xB1, 0xB8, 0xB9, 0xBA,
      0xBB, 0xBC, 0xBD, 0xC0, 0xC3, 0xC8, 0xC9, 0xCC, 0xD0, 0xD3, 0xD8,
      0xD9, 0xDC, 0xE0, 0xF0, 0xF1, 0xF2, 0xF5,
      0xFFF };

    // Read the original world map
		String dataFilename = String.format(worldDataFormat, britanniaFilename);
    final int chunkBytes = CHUNK_SIZE * CHUNK_SIZE;
		byte[] data = new byte[205 * chunkBytes];
    getData(data, getInputStream(dataFilename), true);

    // Open up a new file
    OutputStream worldRecord = new FileOutputStream(worldExpandedFilename);

    // Transcribe & insert
    final byte[] oceanChunk = {
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

    int nextInsertion = 0;
    for (int chunk = 0; chunk < 0x100; chunk++) {
      if(chunk == insertChunks[nextInsertion]) {
        // Write a chunk's worth of ocean bytes (01) to new file
        worldRecord.write(oceanChunk);
        nextInsertion++;
      } else {
        // Copy a chunk's worth of bytes from old file to new
        int offset = (chunk - (nextInsertion)) * chunkBytes;
        worldRecord.write(data, offset, chunkBytes);
      }
    }
    worldRecord.close();
  }

  //** this could probably be moved up to UltimaParser thru use of filename
  // args and putting palette stuff into data
  private void parseWorld(final boolean underworld,
                          final boolean stitch,
                          final boolean generateJs) throws IOException {
    // Get tile images
    BufferedImage tiles = getTileImages();

    // Open the file...
    String filename = worldExpandedFilename;
    if(underworld)
      filename = String.format(worldDataFormat, underworldFilename);
    byte[] data = new byte[MAP_SIZE * MAP_SIZE];
    getData(data, getInputStream(filename), true);

    // Either make a gigantic image or don't
    BufferedImage image = null;
    Writer worldJs = null;
    if (stitch) {
      image = new BufferedImage(MAP_SIZE * TILE_SIZE, 
                                MAP_SIZE * TILE_SIZE, 
                                BufferedImage.TYPE_INT_RGB);
    } else {
      image = new BufferedImage(MAP_SIZE, MAP_SIZE,
                                BufferedImage.TYPE_INT_RGB);
    }

    if (generateJs) {
      worldJs = new BufferedWriter(new FileWriter(underworld ? 
                                                  "u5underworld.js" : 
                                                  "u5world.js"));
      worldJs.write(underworld ? "var map5u=[\n" : "var map5=[\n");
    }

    for (int mapY = 0, index = 0; mapY < MAP_SIZE; ++mapY) {
      if (worldJs != null)
        worldJs.write("[");
      for (int mapX = 0; mapX < MAP_SIZE; ++mapX, ++index) {
        int datum = unsign(data[unchunk(index)]);
        int tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
        int tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
        if (stitch) {
          copyTile(tiles, image, 
                   tileX, tileY, 
                   mapX * TILE_SIZE, mapY * TILE_SIZE, 
                   TILE_SIZE);
        } else {
          // tile 0 is now "hit" so array indices are offset 1
          final int terraColors[] = {
            0x000050,  // deep blue sea
            0x0000A0,  // blue sea
            0x4040A0,  // shallows
            0x007070,  // marsh
            0x000E00,  // grass
            0x004700,  // brush
            0x787810,  // desert
            0x307000,  // scrub forest
            0x408300,  // forest
            0x448A00,  // deep forest
            0x444844,  // hills
            0x8F8F8E,  // mountains
            0xAFAFAE,  // high peak
            0x333833,  // low hills
            0x333833,  // low hills
          };
          int color;
          if (datum <= terraColors.length)
            color = terraColors[datum-1];
          else if (datum == 28)  // desert oasis
            color = 0x505060;
          else if (datum == 30 || datum == 31) // desert edge
            color = 0x454C08;
          else if (datum >= 32 && datum <= 38) // road
            color = 0x303800;
          else if (datum == 47)  // desert cactus
            color = 0x689800;
          else if (datum >= 48 && datum <= 51) // straight coast
            color = terraColors[4]; // i.e. grass
          else if (datum >= 52 && datum <= 55) // diagonal coast
            color = 0x0E1E35;
          else if (datum == 71)  // docks
            color = 0x707840;
          else if (datum == 106 || datum == 107) // bridge over troubled water
            color = 0x484F18;
          else if (datum >= 96 && datum <= 111)  // river
            color = 0x4050B0;
          else if (datum == 137) // cross (underworld)
            color = 0x74740F;
          else if (datum == 143) // a'a-pahoehoe amalgam
            color = 0xC00000;
          else if (datum == 164) // sign
            color = 0xBFCABF;
          else if (datum == 212) // cascade
            color = 0x5854B8;
          else if (datum == 222) // blue flame thing (underworld)
            color = 0x1010FF;
          else if (datum == 255) // utter darkness (underworld)
            color = 0x000000;
          else                   // town or some shit
            color = 0xFFFFFF;
          image.setRGB(mapX, mapY, color);
        }

        if (worldJs != null)
          worldJs.write(datum + ",");
      }
      if (worldJs != null)
        worldJs.write("],\n");
      System.out.print(".");
    }

    filename = underworld ? underworldFilename : britanniaFilename;
    if (stitch) {
      filename = String.format(worldStitchedFormat, filename);
    } else {
      filename = String.format(worldMinimapFormat, filename);
    }
    filename = filename.toLowerCase();
    writeImage(image, filename);
    System.out.println("Wrote " + filename);
    if (worldJs != null) {
      worldJs.write("];\n");
      worldJs.close();
    }

    System.out.println(".");
  }

  //** this also could probably move upstairs with filename params
	private void parseWorldChunks(boolean underworld) {
		// Get tile images
		BufferedImage tiles = getTileImages();
		
		// Open the file...
		String dataFilename = underworld ? underworldFilename : britanniaFilename;
		dataFilename = String.format(worldDataFormat, dataFilename);
		byte[] data = new byte[MAP_SIZE * MAP_SIZE];
		getData(data, getInputStream(dataFilename), true);

		// The map is arranged in chunks of 16x16 tiles.  Underworld has 256.
		// Aboveground has 205 (variously sized rows--skips chunks of all 01s)
		int datum = 0;
		int i = 0;
		int tileX = 0, tileY = 0;
		int mapX, mapY = 0;
		final int chunkCount = (underworld ? 256 : 205);
		for (int chunk = 0; chunk < chunkCount; ++chunk) {
	    // Prepare the chunk image we will draw to -
			BufferedImage image = new BufferedImage(CHUNK_SIZE * TILE_SIZE,
																							CHUNK_SIZE * TILE_SIZE,
																							BufferedImage.TYPE_INT_RGB);
			
	    for (i = 0; i < CHUNK_SIZE * CHUNK_SIZE; ++i) {
				datum = unsign(data[i + chunk * (CHUNK_SIZE * CHUNK_SIZE)]);

				tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
				tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
				mapX = (i % CHUNK_SIZE) * TILE_SIZE;
				mapY = (i / CHUNK_SIZE) * TILE_SIZE;

				copyTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);
	    }

	    // Save image file
			String outFilename = underworld ? underworldFilename : britanniaFilename;
			outFilename = outFilename.toLowerCase();
			outFilename = String.format(worldChunkFormat, outFilename, chunk);
			writeImage(image, outFilename);
			System.out.print(".");
		}
		System.out.println();
	}

  private void parseTowne(final String fileName) throws IOException {
    if (!fileName.equals("DWELLING") &&  // (various)
        !fileName.equals("CASTLE") &&    // (5 LBC, 5 Blackthorne's, 6 villages)
        !fileName.equals("KEEP") &&      // (various)
        !fileName.equals("TOWNE")) {     // (2 per virtue-city)
      System.out.println("Only DWELLING.DAT, CASTLE.DAT, KEEP.DAT, TOWNE.DAT are maps for -m.");
    }

    // Get source images
    BufferedImage tiles = getTileImages();

    // Get data
    final String dataFileName = fileName + towneDataExtension;
    final int TOWNE_SIZE = 32;
    byte[] data = new byte[TOWNE_SIZE * TOWNE_SIZE];
    FileInputStream stream = getInputStream(dataFileName);
    int dataRead = getData(data, stream, false);

    int gridNum = 0;
    do {
      // An image for the next grid's worth of tiles
      BufferedImage image = new BufferedImage(TOWNE_SIZE * TILE_SIZE,
                                              TOWNE_SIZE * TILE_SIZE,
                                              BufferedImage.TYPE_INT_RGB);
      parseTiles(tiles, image, data, 0, TOWNE_SIZE);
      
      // Save image file
      String outFileName = String.format(towneFormat,
                                         U5Data.getTowneName(fileName, gridNum++));
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
      System.out.print(".");

      // Get next batch of data
      dataRead = getData(data, stream, false);
    } while (dataRead == TOWNE_SIZE * TOWNE_SIZE);

    System.out.println();
    stream.close();
  }

  private void parseMiscMaps() {
    // Get source images
    BufferedImage tiles = getTileImages();

    // Get data
    byte[] data = new byte[1871];
    int dataRead = getData(data, getInputStream(miscMapsDataFilename), true);

    // There are 4 11x11 maps, but data is in padded rows of 16 bytes
    for(int i = 0; i < 4; ++i) {
      int mapOffset = i * (16 * 11);
      BufferedImage image = new BufferedImage(11 * TILE_SIZE,
                                              11 * TILE_SIZE,
                                              BufferedImage.TYPE_INT_RGB);
      parsePaddedTiles(tiles, image, data, mapOffset, 11, 11, 5);

      // Save image file
      String outFileName = String.format(miscMapsFormat, i + 1);
      writeImage(image, outFileName);
      System.out.print(".");
    }

    // Then there are 4 19x4 maps, in padded rows of 32 bytes
    for(int i = 0; i < 4; ++i) {
      int mapOffset = (4 * 16 * 11) + (i * (32 * 4));
      BufferedImage image = new BufferedImage(19 * TILE_SIZE,
                                              4 * TILE_SIZE,
                                              BufferedImage.TYPE_INT_RGB);
      parsePaddedTiles(tiles, image, data, mapOffset, 19, 4, 13);

      // Save image file
      String outFileName = String.format(miscMapsFormat, i + 5);
      writeImage(image, outFileName);
      System.out.print(".");
    }
    
    System.out.println("\n(655 remaining bytes uninterpreted.)");
  }

  private static void parseStoryText(final byte[] data,
                                     final int dataLength,
                                     PrintWriter out) {
    final boolean breakSyllables = false; // could be equally irrelevant parameter

    StringBuilder notepad = new StringBuilder();
    char datum = 0;
    for (int i = 0; i < dataLength; i++) {
      datum = (char)unsign(data[i]);

      if (datum == 10 || datum == 0) {
        // some kind of "more" prompt, 'twould seem
        notepad.append(String.format("{%d}", (int)datum));
        out.println(notepad.toString());
        notepad = new StringBuilder();
      }
      else if (datum == 123) {
        // openstache is where new sections begin
        notepad.append("\n**\n");
        out.println(notepad.toString());
        notepad = new StringBuilder();
      }
      else {
        // something else - regular text, presumably

        // Break lines on spaces and syllable breaks
        if (datum == 32 && notepad.length() >= 70) {
          out.println(notepad.toString());
          notepad = new StringBuilder();
        }
        // And beware the double space on new lines!
        else if (datum != 32 || notepad.length() > 0) {
          // There are underscores to break syllables in data.
          if (breakSyllables || datum != 95) {
            // sanity check - other magic codes?
            int charType = Character.getType(datum);
            if (charType != Character.UNASSIGNED &&
                charType != Character.SURROGATE &&
                charType != Character.CONTROL &&
                charType != Character.PRIVATE_USE)
              notepad.append(datum);
            else
              notepad.append(String.format("{%d}", (int)datum));
          }
        }
      }
    }
    out.println(notepad.toString());
  }

  private void parseText(final String fileName) {
    //** There are also .TLK files per map (CASTLE KEEP DWELLING TOWNE),
    // but they are formatted differently from in IV.  Also TOWNE is > 26K,
    // so the buffer size below would not be sufficient.

    // Get the data file
    byte[] data = new byte[16384]; // long enough for anything we expect to find
    int length = getData(data, getInputStream(fileName + textDataExtension), true);

    // Open a text file
    String outFileName = String.format(textFormat, fileName);
    outFileName = outFileName.toLowerCase();
    FileWriter outFile = null;
    try {
      outFile = new FileWriter(outFileName, false);
    } catch (IOException except) {
      System.err.println("Error opening file " + outFileName);
      System.exit(1);
    }
    PrintWriter out = new PrintWriter(outFile);

    if (fileName.equals("END") ||
        fileName.equals("ENDMSG") ||
        fileName.equals("QUESTION") ||
        fileName.equals("STORY") ||
        fileName.equals("KARMA") ||
        //** following entries work increasingly poorly - specialized parsers?
        fileName.equals("MISCMSG") || // normal but for @ etc codes near end
        fileName.equals("SIGNS") || // the @ space + sign-drawing codes + ?
        fileName.equals("SHOPPE") || // various magic codes for common words
        fileName.equals("LOOK2")) { // raw data precedes the main entries
      parseStoryText(data, length, out);
    } else {
      System.out.println(fileName + " not (yet?) supported.");
    }

    out.close();
  }

  private static void printInstructions() {
		PrintStream o = System.out;
    o.println("U5Parser function depends on option given:");
    o.println("  U5Parser -g : # Generates graphics tile bitmap from u5tiles.16 (from TILES.16)");
		/*
    o.println("  U5Parser -f : # Generates font bitmap from u5text.16 (from TEXT.16)");
    */
    o.println("  U5Parser -expand : Builds u5fullbrit.data from BRIT.DAT");
    o.println("  U5Parser -mini : * Generates minimap from u5fullbrit.dat (from BRIT.DAT)");
    o.println("  U5Parser -w : *? Generates Javascript describing world map");
    o.println("  U5Parser -ws : * Generates bitmap of world map from FULLBRIT.DAT (requires large heap)");
    o.println("  U5Parser -wc : * Generates several bitmaps of world map chunks from BRIT.DAT");
    o.println("  U5Parser -umini : * Generates minimap from UNDER.DAT");
    o.println("  U5Parser -u : *? Generates Javascript describing underworld map");
    o.println("  U5Parser -us : * Generates bitmap of underworld map from UNDER.DAT (requires large heap)");
    o.println("  U5Parser -uc : * Generates several bitmaps of underworld map chunks from UNDER.DAT");
/*
    o.println("  U5Parser -c [FILE] : * Generates bitmaps of conflict map from [FILE].CON");
*/
    o.println("  U5Parser -d : * Generates bitmaps of dungeon maps from DUNGEON.DAT");
    o.println("  U5Parser -m [FILE] : * Generates bitmaps of maps from [FILE].DAT\n\t\t(Where [FILE] is one of { dwelling, castle, keep, towne })");
    o.println("  U5Parser -miscmaps : * Generates bitmaps of maps from MISCMAPS.DAT");
    o.println("  U5Parser -t [FILE] : Generates text from [FILE].DAT where [FILE] is one of\n\t\t{ end, endmsg, karma, look2, miscmsg, question, shoppe, signs, story }");
		o.println(" # : These options require decompression via u6decode");
    o.println(" * : These options require the bitmaps from -g and/or -f options");
  }

  private boolean eatArgs(String[] args) throws IOException {
    if (!eatTestingArgs(args)) {
      final String modeArg = args[0].toLowerCase();
      if (modeArg.equals("-g"))
        parseEga(tileDataFilename, getTileFilename(), 512, TILE_SIZE, 0);
      else if (modeArg.equals("-cga")) // MERRY EASTER!!!
        parseCga(tileCGADataFilename, tileCGAFilename, 512, TILE_SIZE, false);

      else if (modeArg.equals("-test") && args.length > 1) // peek at *.16
        parseEga(args[1], "u5test.png", 512, 16, 0);
      else if (modeArg.equals("-grind") && args.length > 2) // peek at *.DAT
        grindMapData(args[1], new Integer(args[2]).intValue());
      else if (modeArg.equals("-slice") && args.length > 3) // peek at *.DAT
        sliceMapData(args[1],
                     new Integer(args[2]).intValue(),
                     new Integer(args[3]).intValue());
      else if (modeArg.equals("-run") && args.length > 1) // seek padding
        printDataRuns(args[1].toUpperCase());

      else if (modeArg.equals("-f"))
        // something not quite straightforward about the font file - 
        // seems to start with a palette and then..? vertical runs??
        parseEga(fontDataFilename, getFontFilename(), 384, FONT_SIZE, 0);
        //parseEga(fontDataFilename, fontFilename, 384, FONT_SIZE, 0x2000);//0x28A0 - 64);
        /*
        for (int i = 0; i < 33; ++i)
          parseEga(fontDataFilename, String.format("u5font%02d.png", i), 384, FONT_SIZE, i);
        */
      else if (modeArg.equals("-cgaf")) // etc
        //parseCga(fontCGADataFilename, fontCGAFilename, 128, FONT_SIZE);
        // equally borked
        parseCga(fontCGADataFilename, fontCGAFilename, 384, FONT_SIZE, false);
      /*
      else if (modeArg.equals("-c") && args.length > 1)
        parseConflict(args[1].toUpperCase());
      */
      else if (modeArg.equals("-d"))
        parseDungeon();
      else if (modeArg.equals("-umini"))
        parseWorld(true, false, false);
      else if (modeArg.equals("-u"))
        parseWorld(true, false, true);
      else if (modeArg.equals("-us"))
        parseWorld(true, true, false);
      else if (modeArg.equals("-mini"))
        parseWorld(false, false, false);
      else if (modeArg.equals("-w"))
        parseWorld(false, false, true);
      else if (modeArg.equals("-ws"))
        parseWorld(false, true, false);
      else if (modeArg.equals("-wc"))
        parseWorldChunks(false);
      else if (modeArg.equals("-uc"))
        parseWorldChunks(true);
      else if (modeArg.equals("-expand"))
        expandWorldMap();
      else if (modeArg.equals("-m") && args.length > 1)
        parseTowne(args[1].toUpperCase());
       else if (modeArg.equals("-miscmaps"))
        parseMiscMaps();
      else if (modeArg.equals("-t") && args.length > 1)
        parseText(args[1].toUpperCase());
      else
        return false;
    }
    return true;
  }

  public static void main(String[] args) throws IOException {
    boolean eaten = false;
    if (args.length > 0)
      eaten = new U5Parser().eatArgs(args);
    if (!eaten)
      printInstructions();
  }
}
