VPATH = ULTIMA4 ULTIMAV

default: u4minimap.png u4font.png u4tiles.png u4world.js u5world.js u5underworld.js

u5all: u5britminimap.png u5underminimap.png u5world.js u5underworld.js u5tiles.png

EVERYCUNT = ($! (cd ULTIMA4; ls *.EGA))

# These are convertable
all:	CHARSET.png \
	COMPASSN.png \
	COURAGE.png \
	HONESTY.png \
	HONOR.png \
	HUMILITY.png \
	JUSTICE.png \
	KEY7.png \
	LOVE.png \
	RUNE_0.png \
	RUNE_1.png \
	RUNE_2.png \
	RUNE_3.png \
	RUNE_4.png \
	RUNE_5.png \
	SACRIFIC.png \
	SHAPES.png \
	SPIRIT.png \
	START.png \
	STONCRCL.png \
	TRUTH.png \
	VALOR.png

# These are not
borken:	ABACUS.png \
	ANIMATE.png \
	GYPSY.png \
	HONCOM.png \
	INSIDE.png \
	OUTSIDE.png \
	PORTAL.png \
	SACHONOR.png \
	SPIRHUM.png \
	TITLE.png \
	TREE.png \
	VALJUS.png \
	WAGON.png 

# These even less so
badlyborken:
	rune_6.png \
	rune_7.png \
	rune_8.png

%.png: %.EGA U4Parser.class
	java U4Parser $< $@

%.class: %.java
	javac $<

u4world.js: U4Parser.class ULTIMA4/WORLD.MAP
	java U4Parser -w

u4minimap.png: U4Parser.class ULTIMA4/WORLD.MAP
	java U4Parser -mini

u4world.png: U4Parser.class ULTIMA4/WORLD.MAP u4tiles.png
	java -Xmx20g U4Parser -ws

u4tiles.png: U4Parser.class ULTIMA4/SHAPES.EGA
	java U4Parser -g

u4font.png: U4Parser.class ULTIMA4/CHARSET.EGA
	java U4Parser -f

ULTIMA4/WORLD.MAP: ULTIMA4
ULTIMA4/SHAPES.EGA: ULTIMA4
ULTIMA4/CHARSET.EGA: ULTIMA4

ULTIMAV/TILES.16: ULTIMAV
ULTIMAV/UNDER.DAT: ULTIMAV
ULTIMAV/BRIT.DAT: ULTIMAV

ULTIMA4:
	curl -O http://grumdrig.com/dl/UltimaIV.zip
	unzip UltimaIV.zip

ULTIMAV:
	curl -O http://grumdrig.com/dl/UltimaV.zip
	unzip UltimaV.zip


publish: index.html u4tiles.png u4font.png u4world.js u4minimap.png reveng.html u5tiles.png u5britminimap.png u5underminimap.png u5world.js u5underworld.js u5look.js ibm_ch.js runes_ch.js signs.js
	rsync -v $^ grumdrig.com:www/grumdrig.com/u4map/

u5/u6decode/u6decode:
	cd u5/u6decode
	make

u5tiles.png: u5/u6decode/u6decode ULTIMAV/TILES.16 U5Parser.class
	u5/u6decode/u6decode ULTIMAV/TILES.16 u5tiles.16
	java U5Parser -g

u5fullbrit.dat: ULTIMAV/BRIT.DAT U5Parser.class
	java U5Parser -expand

u5britminimap.png: u5fullbrit.dat U5Parser.class
	java U5Parser -mini

u5underminimap.png: ULTIMAV/UNDER.DAT U5Parser.class
	java U5Parser -umini

u5world.js: u5tiles.png U5Parser.class u5fullbrit.dat
	java U5Parser -w

u5underworld.js: U5Parser.class ULTIMAV/UNDER.DAT
	java U5Parser -u

u5look.js: ULTIMAV/LOOK2.DAT look2.con
	node ../decon/decon.js -V u5look look2.con Look2 -i ULTIMAV/LOOK2.DAT -o u5look.js

ibm_ch.js: ch.con ULTIMAV/IBM.CH
	node ../decon/decon.js -V ibm_ch ch.con Main -i ULTIMAV/IBM.CH -o ibm_ch.js

runes_ch.js: ch.con ULTIMAV/RUNES.CH
	node ../decon/decon.js -V runes_ch ch.con Main -i ULTIMAV/RUNES.CH -o runes_ch.js

signs.js: signs.con ULTIMAV/SIGNS.DAT
	node ../decon/decon.js -p -V signs -f groups signs.con Signs -i ULTIMAV/SIGNS.DAT -o signs.js
