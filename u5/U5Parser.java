import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.*;

class U5Parser {

  private static int[] egaPalette = {
    0x000000,
    0x000080,
    0x008000,
    0x008080,
    0x800000,
    0x800080,
    0x808000,
    0xC0C0C0,
    0x808080,
    0x0000FF,
    0x00FF00,
    0x00FFFF,
    0xFF4040,
    0xFF00FF,
    0xFFFF00,
    0xFFFFFF };

  private static String dataFilename = "u5tiles.16";
  private static String tileFilename = "u5tiles.png";

  private static void parseEga(String fileName) {
    // Open data file
    FileInputStream stream = null;
    try {
      stream = new FileInputStream(fileName);
    } catch (FileNotFoundException except) {
      System.err.println("FNFE");
      System.exit(1);
    }

    // Read data file
    byte[] data = new byte[512 * 16 * 16 / 2];
    try {
      stream.read(data);
      stream.close();
    } catch (IOException except) {
      System.err.println("IOE");
      System.exit(1);
    }

    // Write data to image
    BufferedImage image = new BufferedImage(512, 256, BufferedImage.TYPE_INT_RGB);
    int datum = 0;
    int tileNum = 0, tileOff = 0;
    int pixelX = 0, pixelY = 0;
    for (int i = 0; i < data.length; i++) {
      // Compensate for the sign of the byte (2s complement)
      datum = 0xFF & (int)data[i];
      tileNum = i / 128;
      tileOff = i % 128;

      pixelX = ((tileNum % 32) * 16 + (tileOff+tileOff) % 16);
      pixelY = ((tileNum / 32) * 16 + (tileOff+tileOff) / 16);

      image.setRGB(pixelX, pixelY, U5Parser.egaPalette[(datum & 0xF0) >>> 4]);
      image.setRGB(pixelX + 1, pixelY, U5Parser.egaPalette[datum & 0x0F]);
    }

    // Save image file
    try {
      ImageWriter writer = ImageIO.getImageWritersByFormatName("png").next();
      File outFile = new File(U5Parser.tileFilename);
      javax.imageio.stream.ImageOutputStream ios = ImageIO.createImageOutputStream(outFile);
      writer.setOutput(ios);
      writer.write(image);
    } catch (IOException except) {
      System.err.println("IOE");
      System.exit(1);
    }
  }

  public static void main(String[] args) {
    // some day worry about other data files etc
    String fileName = U5Parser.dataFilename; // or read args etc.
    parseEga(fileName);
  }
}


