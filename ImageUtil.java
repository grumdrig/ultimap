import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.*;


class ImageUtil {

	public static boolean saveImage(BufferedImage image, final String fileName) {
    // Save image file
    try {
			ImageIO.write(image,
										fileName.substring(fileName.length() - 3),
										new File(fileName));
    } catch (Exception except) {
			except.printStackTrace();
			return false;
    }
		return true;
  }

	public static BufferedImage loadImage(final String fileName) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File(fileName));
		} catch (Exception except) {
			except.printStackTrace();
		}
		return image;
	}
}

