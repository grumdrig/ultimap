import java.io.FileInputStream;
import java.awt.image.BufferedImage;
import javax.imageio.*;


class U4RLEParser {

  private static int[] egaPalette = {
    0x000000,
    0x000080,
    0x008000,
    0x008080,
    0x800000,
    0x800080,
    0x808000,
    0xC0C0C0,
    0x808080,
    0x0000FF,
    0x00FF00,
    0x00FFFF,
    0xFF4040,
    0xFF00FF,
    0xFFFF00,
    0xFFFFFF };

  private static void parseEga(String fileName,
                               String outputFilename) {
    System.out.println(fileName + " -> " + outputFilename);

    // Open data file
    FileInputStream stream = null;
    try {
      stream = new FileInputStream(fileName);
    } catch (java.io.FileNotFoundException except) {
      System.err.println("FNFE");
      System.exit(1);
    }

    // Read data file
    byte[] data = new byte[256 * 16 * 16 / 2];
    int red;
    try {
      red = stream.read(data);
      stream.close();
    } catch (java.io.IOException except) {
      System.err.println("IOE");
      System.exit(1);
      return;
    }


    int gridSize = 16;
    int tileSize = 0;
    int imageWidth = 320;
    int imageHeight = 200;
    if (red == 0x8000) {
      tileSize = 16;
      imageWidth = imageHeight = gridSize * tileSize;
    } else if (red == 0x2000) {
      tileSize = 8;
      imageWidth = imageHeight = gridSize * tileSize;
    }


    // Write data to image
    // (One wonders whether one might have done interesting things using an IndexColorModel...)
    //BufferedImage image = new BufferedImage(256, 256, BufferedImage.TYPE_BYTE_INDEXED, icModel);
    BufferedImage image = new BufferedImage(imageWidth,
                                            imageHeight, 
                                            BufferedImage.TYPE_INT_RGB);

    if (tileSize > 0) {
      // Put tiles into a grid
      for (int i = 0; i < red; i++) {
        // Compensate for the sign of the byte (2s complement)
        int datum = 0xFF & (int)data[i];
        int tileNum = i / (tileSize * tileSize / 2);
        int tileOff = i % (tileSize * tileSize / 2);
        int pixelX = (tileNum % 16) * tileSize + (2*tileOff) % tileSize;
        int pixelY = (tileNum / 16) * tileSize + (2*tileOff) / tileSize;
        
        image.setRGB(pixelX, pixelY, egaPalette[(datum & 0xF0) >>> 4]);
        image.setRGB(pixelX + 1, pixelY, egaPalette[datum & 0x0F]);
      }
    } else {
      // Un-RLE into a screen-sized image
      int x = 0, y = 0;
      for (int i = 0; i < red; i++) {
        int length = 1, value = 0xFF & (int)data[i];
        if (value == 2) {
          // Signifies a run
          length = 0xFF & (int)data[++i];
          value = 0xFF & (int)data[++i];
        }
        while (length-- > 0) {
          image.setRGB(x++, y, egaPalette[(value >>> 4) & 0xF]);
          image.setRGB(x++, y, egaPalette[value & 0xF]);
          if (x >= imageWidth) {
            x = 0;
            ++y;
          }
        }
        
      }
    }

    // Save image file
    try {
      ImageWriter writer = ImageIO.getImageWritersByFormatName("png").next();
      java.io.File outFile = new java.io.File(outputFilename);
      javax.imageio.stream.ImageOutputStream ios = ImageIO.createImageOutputStream(outFile);
      writer.setOutput(ios);
      writer.write(image);
    } catch (java.io.IOException except) {
      System.err.println("IOE");
      System.exit(1);
    }
  }

  public static void main(String[] args) {
    parseEga(args[0], args[1]);
  }
}


