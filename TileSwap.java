import java.awt.image.BufferedImage;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
//import javax.swing.event.*;


class TileSwap {
	//Liberal initial use made of code found at:
	//http://www.javalobby.org/articles/ultimate-image/

    /*
    BufferedImage image = new BufferedImage(imageWidth,
                                            imageHeight, 
                                            BufferedImage.TYPE_INT_RGB);

    if (tileSize > 0) {
      // Put tiles into a grid
      for (int i = 0; i < red; i++) {
        // Compensate for the sign of the byte (2s complement)
        int datum = 0xFF & (int)data[i];
        int tileNum = i / (tileSize * tileSize / 2);
        int tileOff = i % (tileSize * tileSize / 2);
        int pixelX = (tileNum % 16) * tileSize + (2*tileOff) % tileSize;
        int pixelY = (tileNum / 16) * tileSize + (2*tileOff) / tileSize;
        
        image.setRGB(pixelX, pixelY, egaPalette[(datum & 0xF0) >>> 4]);
        image.setRGB(pixelX + 1, pixelY, egaPalette[datum & 0x0F]);
      }
    }
  }
    */

	// Deserves its own file I suppose.
	public class TileImagePanel extends JPanel implements Scrollable, MouseListener {
		private BufferedImage image;
		private String outFileName;

		//int x, y;

		Point tileSelection = null;
		Point mouseSelection = null;

		//public TileImagePanel(BufferedImage image, int x, int y) {  
		public TileImagePanel(BufferedImage image, String outputName) {
			super();
			this.image = image;
			//this.x = x;
			//this.y = y;
			this.outFileName = outputName;

			addMouseListener(this);
		}  

		protected void paintComponent(Graphics g) {  
			super.paintComponent(g);
			//g.drawImage(image, x, y, null);
			g.drawImage(image, 0, 0, null);
			// ** add colored boxes to show which tiles are selected...
		}

		private Point getTileFromMouse(final int x, final int y) {
			return new Point(x / 16, y / 16);
		}

		private void selectTile(final Point tile) {
			if(tileSelection == null) {
				// first tile - draw a box or whatever
				tileSelection = tile;
			} else {
				if(!tileSelection.equals(tile)) {
					// selected 2nd tile - swappy swappy
					System.out.println(String.format("swap %d,%d with %d, %d",
																					 tileSelection.x,
																					 tileSelection.y,
																					 tile.x,
																					 tile.y));
					swapTiles(tile);
				}

				// clear box, unselect
				tileSelection = null;
			}
		}

		// mostly works except it adds little diagonal lines
		private void swapTiles(Point tile2) {
			Point tile1 = this.tileSelection;

			tile1.x *= 16;
			tile1.y *= 16;
			tile2.x *= 16;
			tile2.y *= 16;

			// Try this..
			BufferedImage cache = new BufferedImage(16, 16, this.image.getType());
			Graphics2D g1 = cache.createGraphics();
			g1.drawImage(this.image, 0, 0, 16, 16, tile1.x, tile1.y, tile1.x + 16, tile1.y + 16, null);

			Graphics g2 = image.getGraphics();
			//Graphics2D g2 = this.image.createGraphics();
			g2.drawImage(this.image, tile1.x, tile1.y, tile1.x + 16, tile1.y + 16, tile2.x, tile2.y, tile2.x + 16, tile2.y + 16, null);

			g2.drawImage(cache, tile2.x, tile2.y, tile2.x + 16, tile2.y + 16, 0, 0, 16, 16, null);

			g1.dispose();
			g2.dispose();

			Dimension dim = new Dimension(16,16);
			this.repaint(new Rectangle(tile1, dim));
			this.repaint(new Rectangle(tile2, dim));
		}

		// SCROLLABLE
		public Dimension getPreferredScrollableViewportSize() {
			return getPreferredSize();
		}

		public int getScrollableBlockIncrement(Rectangle visibleRect,
																					 int orientation,
																					 int direction) {
			// if showing a partial row (top or bottom), give the offset
			return 16;
		}

		public boolean getScrollableTracksViewportHeight() { return false; }
		public boolean getScrollableTracksViewportWidth() { return false; }

		public int getScrollableUnitIncrement(Rectangle visibleRect,
																	 int orientation,
																	 int direction) {
			// if showing a partial row (top or bottom), give the offset
			return 16;
		}

		// MOUSELISTENER
		public void mouseClicked(MouseEvent e) {
			if(e.getButton() == MouseEvent.BUTTON2 || e.getButton() == MouseEvent.BUTTON3) {
				System.out.println("save!");
				// now we're talkin'
				ImageUtil.saveImage(image, outFileName);
			}
		}

		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}

		public void mousePressed(MouseEvent e) {
			if(e.getButton() == MouseEvent.BUTTON1) {
				//System.out.println("down " + e.getX() + "," + e.getY());

				if(mouseSelection != null)
					System.err.println("unexpected! got another mouse down.");
				mouseSelection = new Point(e.getX(), e.getY());
			}
		}

		public void mouseReleased(MouseEvent e) {
			if(e.getButton() == MouseEvent.BUTTON1) {
				//System.out.println("up " + e.getX() + "," + e.getY());

				if(mouseSelection == null)
					System.err.println("unexpected! get another mouse up.");

				Point prevTile = getTileFromMouse(mouseSelection.x, mouseSelection.y);
				if(prevTile.equals(getTileFromMouse(e.getX(), e.getY())))
					selectTile(prevTile);

				mouseSelection = null;
			}
		}
	}  

	private void showTiles(JFrame frame, final String inFile, final String outFile) {
		BufferedImage image = ImageUtil.loadImage(inFile);
		if(image != null) {
			//TileImagePanel panel = new TileImagePanel(image, 0, 0);
			TileImagePanel panel = new TileImagePanel(image, outFile);
			//** blimey.  somewhere around here is a carefully guarded secret of how to not cover up the right side of the picture with a scrollbar
			panel.setPreferredSize(new Dimension(image.getWidth() + 50, image.getHeight()));
			JScrollPane scroller = new JScrollPane(panel,
																						 ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
																						 //ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
																						 ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			frame.setBounds(0, 0, //550, 600);
											image.getWidth() + scroller.getVerticalScrollBar().getWidth(),
											Math.min(image.getHeight(), 512));
			//panel.setPreferredSize(new Dimension(image.getWidth() + scroller.getVerticalScrollBar().getWidth(), Math.min(image.getHeight(), 512)));
			//panel.setPreferredSize(new Dimension(600, 1024));
			panel.setPreferredSize(new Dimension(image.getWidth() + scroller.getVerticalScrollBar().getWidth(), image.getHeight()));
			frame.setVisible(true);
			frame.add(scroller);
		}
	}

	public static void main(String[] args) {
		if(args.length > 1) {
			TileSwap app = new TileSwap();
			JFrame frame = new JFrame("Tile Swapper");
			frame.addWindowListener(new WindowAdapter() {  
					public void windowClosing(WindowEvent event) {  
						System.exit(0);  
					}  
				});
			app.showTiles(frame, args[0], args[1]);
		}
		else
			System.out.println("First arg is tiled image name, second is where to save modifications.");
	}
}


