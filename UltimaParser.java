import java.io.*;
import java.awt.image.BufferedImage;


abstract class UltimaParser {
  
  protected static final int[] egaPalette = {
    0x000000,
    0x000080,
    0x008000,
    0x008080,
    0x800000,
    0x800080,
    0x808000,
    0xC0C0C0,
    0x808080,
    0x0000FF,
    0x00FF00,
    0x00FFFF,
    0xFF4040,
    0xFF00FF,
    0xFFFF00,
    0xFFFFFF };

  protected static final int[] cgaPalette = {
    //0x000000, 0x00AA00, 0xAA0000, 0xAA5500 }; // Palette 0
    //0x000000, 0x55FF55, 0xFF5555, 0xFFFF55 }; // Palette 0 (high intensity)
    //0x000000, 0x00AAAA, 0xAA00AA, 0xAAAAAA }; // Palette 1
    0x000000, 0x55FFFF, 0xFF55FF, 0xFFFFFF }; // Palette 1 (high intensity)
  //0x000000, 0x00AAAA, 0xAA0000, 0xAAAAAA }; // Palette #3
  //0x000000, 0x55FFFF, 0xFF5555, 0xFFFFFF }; // Palette #3 (high intensity)


  protected static int MAP_SIZE = 256;
  protected static int CHUNK_SIZE = 32;
  protected static int TILE_SIZE = 16;
  protected static int FONT_SIZE = 8;
  protected static int TEXTURE_TILE_WIDTH = 16;

  protected abstract String getDataDir();


  /*
  private static final String tileDataFilename = "./u5tiles.16"; // from TILES.16
  */
  //private static final String tileFilename = "u5tiles.png";
  protected abstract String getTileFilename();
  /*
  private static final String tileCGADataFilename = "u5tiles.4"; // from TILES.4
  private static final String tileCGAFilename = "u5cga.png";

  private static final String fontDataFilename = "u5text.16"; // from TEXT.16
  */
  //private static final String fontFilename = "u5font.png";
  protected abstract String getFontFilename();
  /*
  private static final String fontCGADataFilename = "u5text.4"; // from TEXT.4
  private static final String fontCGAFilename = "u5cgaf.png";

  private static final String britanniaFilename = "BRIT";
  private static final String underworldFilename = "UNDER";
  private static final String worldDataFormat = "%s.DAT";
  private static final String worldExpandedFilename = "./u5fullbrit.dat";
  private static final String worldStitchedFormat = "u5%s.png";
  //private static final String worldChunkFormat = "u5%s%02o.png";
  private static final String worldChunkFormat = "u5%s%d.png";
  private static final String worldMinimapFormat = "u5%sminimap.png";

  private static final String towneDataExtension = ".DAT";
  private static final String towneFormat = "u5map%s.png";

  private static final String miscMapsDataFilename = "MISCMAPS.DAT";
  private static final String miscMapsFormat = "u5miscmaps%d.png";
  
  private static final String textDataExtension = ".DAT";
  private static final String textFormat = "u5txt%S.txt";
  */  /*
  private static final String conflictDataExtension = ".CON";
  private static final String conflictFormat = "u4con%S.png";
  private static final String conflictSpawnFormat = "u4con%Ss.png";
      *//*
  private static final String dungeonDataFilename = "DUNGEON.DAT";
  private static final String dungeonFormat = "u5dng%s%d.png";
  //private static final String dungeonRoomFormat = "u5dng%Sr%02d.png";
  //private static final String dungeonRoomSpawnFormat = "u5dng%Sr%02ds.png";
  //private static final String dungeonRoomTriggerFormat = "u5dng%Sr%02dt.png";
  */


  public static int unsign(byte b) {
    // Bytes are signed 2's complement-style.
    // There is probably a really fantastic reason that bytes are signed.
    // Some day I will be old enough to understand.
    return 0xFF & (int)b;
  }

  protected FileInputStream getInputStream(String fileName) {
    if (fileName.indexOf('/') == -1)
      fileName = getDataDir() + fileName;
    FileInputStream stream = null;
    try {
      stream = new FileInputStream(fileName);
    } catch (FileNotFoundException except) {
      System.err.println("Could not find data file: " + fileName);
      System.exit(1);
    }
    return stream;
  }
  
  protected static int getData(byte[] data,
                               FileInputStream stream,
                               boolean close) {
    int bytesRead = 0;
    try {
      bytesRead = stream.read(data);
      if (close)
        stream.close();
    } catch (IOException except) {
      System.err.println("Could not read data file");
      System.exit(1);
    }
    return bytesRead;
  }
  
  //** with this superclass, maybe ImageUtil has become overkill?
  protected static void writeImage(final BufferedImage image,
                                   final String fileName) {
		if (!ImageUtil.saveImage(image, fileName)) {
      System.err.println("Could not write to file: " + fileName);
      System.exit(1);
    }
  }

  protected static BufferedImage getImageFile(final String fileName) {
		BufferedImage image = ImageUtil.loadImage(fileName);
		if (image == null) {
      System.err.println("Error reading file: " + fileName);
      System.exit(1);
    }
    return image;
  }

  protected BufferedImage getTileImages() {
    return getImageFile(getTileFilename());
  }

  protected BufferedImage getFontImages() {
    return getImageFile(getFontFilename());
  }

  private void parseGraphics(final String dataFileName,
                             final String outFileName,
                             final int numTiles,
                             final int tileDim,
                             final String mode,
                             // skipBytes part of font-parsing trials
                             final int skipBytes) {
    final int pixelsPerByte = mode.equals("ega") ? 2 : 4;

    // This function just exists because there were 2 very similar ones...
    if (pixelsPerByte != 2 && pixelsPerByte != 4) {
      System.err.println("Unsupported graphics format.");
      System.exit(1);
    }

    // Get data
    byte[] data = new byte[numTiles * tileDim * tileDim / pixelsPerByte];
    getData(data, getInputStream(dataFileName), true);
    //System.out.println("data size " + data.length);

    // Write data to image - always 16 tiles across
    final int imageWidth = tileDim * TEXTURE_TILE_WIDTH;
    int imageHeight = (numTiles / TEXTURE_TILE_WIDTH) * tileDim;
    if (numTiles % TEXTURE_TILE_WIDTH != 0)
      imageHeight += tileDim;
    BufferedImage image = new BufferedImage(imageWidth,
                                            imageHeight,
                                            BufferedImage.TYPE_INT_RGB);
    int datum = 0;
    int tileNum = 0, tileOff = 0;
    int pixelX = 0, pixelY = 0;
    final int bytesPerTile = tileDim * tileDim / pixelsPerByte;
    //for (int i = 0; i < data.length; i++) {
    for (int i = skipBytes; i < data.length; i++) {
      // Compensate for the signed byte (2s complement)
      datum = unsign(data[i]);

      //tileNum = i / bytesPerTile;
      //tileOff = i % bytesPerTile;
      tileNum = (i - skipBytes) / bytesPerTile;
      tileOff = (i - skipBytes) % bytesPerTile;

      // Get x,y of first pixel in byte
      pixelX = ((tileNum % TEXTURE_TILE_WIDTH) * tileDim) +
				((tileOff << (pixelsPerByte / 2)) % tileDim);

      pixelY = (tileOff << (pixelsPerByte / 2)) / tileDim;
      if (mode.equals("cgai")) {
        // Interlaced graphics
        if (pixelY < tileDim / 2)
          pixelY += pixelY;
        else
          pixelY = 1 + (pixelY - tileDim / 2) * 2;
      }
      pixelY += (tileNum / TEXTURE_TILE_WIDTH) * tileDim;

      // Draw, podner
      if (pixelsPerByte == 2) {
        image.setRGB(pixelX, pixelY, egaPalette[(datum & 0xF0) >>> 4]);
        image.setRGB(pixelX + 1, pixelY, egaPalette[datum & 0x0F]);
      }
      else if (pixelsPerByte == 4) {
        image.setRGB(pixelX, pixelY, cgaPalette[(datum >> 6) & 0x3]);
        image.setRGB(pixelX + 1, pixelY, cgaPalette[(datum >> 4) & 0x3]);
        image.setRGB(pixelX + 2, pixelY, cgaPalette[(datum >> 2) & 0x3]);
        image.setRGB(pixelX + 3, pixelY, cgaPalette[datum & 0x03]);
      }
    }

    // Save image file
    writeImage(image, outFileName);
  }

  protected void parseEga(final String dataFileName,
                          final String outFileName,
                          final int numTiles,
                          final int tileDim,
                          final int skipBytes) {
    parseGraphics(dataFileName, outFileName, numTiles, tileDim, "ega", skipBytes);
  }

  protected void parseCga(final String dataFileName,
                          final String outFileName,
                          final int numTiles,
                          final int tileDim,
                          final boolean interlaced) {
    final String mode = interlaced ? "cgai" : "cga";
    parseGraphics(dataFileName, outFileName, numTiles, tileDim, mode, 0);
  }

  protected static void copyTile(BufferedImage source,
                                 BufferedImage dest,
                                 final int sourceX,
                                 final int sourceY,
                                 final int destX,
                                 final int destY,
                                 final int tileDim) {
		java.awt.Graphics2D g = dest.createGraphics();
		g.drawImage(source,
								destX, destY, destX + tileDim, destY + tileDim,
								sourceX, sourceY, sourceX + tileDim, sourceY + tileDim,
								null);
  }

  protected static void copyMaskedTile(BufferedImage source,
                                       BufferedImage dest,
                                       final int sourceX,
                                       final int sourceY,
                                       final int destX,
                                       final int destY,
                                       final int tileDim) {
		// Always taking black as the mask.
		// It might seem better to do a Graphics.drawImage() first and
		// then apply the mask, but my assumption is that most of the time
		// this is called, less than half the pixels in a tile are opaque.
    int rgb = 0;
		final int black = java.awt.Color.BLACK.getRGB();
    for (int x = 0; x < tileDim; ++x) {
      for (int y = 0; y < tileDim; ++y) {
        // Skip the black pixels
        rgb = source.getRGB(x + sourceX, y + sourceY);
        if (rgb != black)
          dest.setRGB(x + destX, y + destY, rgb);
      }
    }
  }

  protected static void parseTiles(final BufferedImage source,
                                   BufferedImage dest,
                                   final byte[] data,
                                   final int dataOffset,
                                   final int squareDim) {
    // This assumes it's getting a square's worth of tile data.
    // Each byte in data refers to a 16x16 tile in source to be copied into dest.
    // The source image is assumed to be an array of such tiles, 16 tiles per row.
    int datum = 0;
    int tileX = 0, tileY = 0;
    int mapX = 0, mapY = 0;
    for (int i = 0; i < squareDim * squareDim; ++i) {
      datum = unsign(data[i + dataOffset]);

      tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
      tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
      mapX = (i % squareDim) * TILE_SIZE;
      mapY = (i / squareDim) * TILE_SIZE;

      copyTile(source, dest, tileX, tileY, mapX, mapY, TILE_SIZE);
    }
  }
  
  protected static void parsePaddedTiles(final BufferedImage source,
                                         BufferedImage dest,
                                         final byte[] data,
                                         final int dataOffset,
                                         final int rectWidth,
                                         final int rectHeight,
                                         final int endRowPadding) {
    // Build a rectangle of tile data, allowing each row of data to
    // terminate with a certain number of bytes which are ignored.
    // Otherwise works like parseTiles does.
    int datum = 0, rowOffset = 0;
    int tileX = 0, tileY = 0;
    int mapX = 0, mapY = 0;
    for (int y = 0; y < rectHeight; y++) {
      rowOffset = (rectWidth + endRowPadding) * y;
      mapY = y * TILE_SIZE;
      for (int x = 0; x < rectWidth; x++) {
        mapX = x * TILE_SIZE;
        datum = unsign(data[dataOffset + rowOffset + x]);
        tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
        tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;

        copyTile(source, dest, tileX, tileY, mapX, mapY, TILE_SIZE);
      }
    }
  }

  protected static int unchunk(int i) {
    // Convert an index into a flat array into an index into
    // chunk-ordered array in the WORLD.MAP schema
    int x = i % MAP_SIZE, y = i / MAP_SIZE;
    int chunk = x/CHUNK_SIZE + (MAP_SIZE/CHUNK_SIZE) * (y/CHUNK_SIZE);
    int dx = x % CHUNK_SIZE, dy = y % CHUNK_SIZE;
    return chunk * CHUNK_SIZE * CHUNK_SIZE + dy * CHUNK_SIZE + dx;
  }
  /*
  private static void parseWorld(final boolean underworld,
                                 final boolean stitch,
                                 final boolean generateJs) throws IOException {
    // Get tile images
    BufferedImage tiles = getTileImages();

    // Open the file...
    String filename = worldExpandedFilename;
    if(underworld)
      filename = String.format(worldDataFormat, underworldFilename);
    byte[] data = new byte[MAP_SIZE * MAP_SIZE];
    getData(data, getInputStream(filename), true);

    // Either make a gigantic image or don't
    BufferedImage image = null;
    Writer worldJs = null;
    if (stitch) {
      image = new BufferedImage(MAP_SIZE * TILE_SIZE, 
                                MAP_SIZE * TILE_SIZE, 
                                BufferedImage.TYPE_INT_RGB);
    } else {
      image = new BufferedImage(MAP_SIZE, MAP_SIZE,
                                BufferedImage.TYPE_INT_RGB);
    }

    if (generateJs) {
      worldJs = new BufferedWriter(new FileWriter(underworld ? 
                                                  "u5underworld.js" : 
                                                  "u5world.js"));
      worldJs.write("var map=[\n");
    }

    for (int mapY = 0, index = 0; mapY < MAP_SIZE; ++mapY) {
      if (worldJs != null)
        worldJs.write("[");
      for (int mapX = 0; mapX < MAP_SIZE; ++mapX, ++index) {
        int datum = unsign(data[unchunk(index)]);
        int tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
        int tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
        if (stitch) {
          copyTile(tiles, image, 
                   tileX, tileY, 
                   mapX * TILE_SIZE, mapY * TILE_SIZE, 
                   TILE_SIZE);
        } else {
          // tile 0 is now "hit" so array indices are offset 1
          final int terraColors[] = {
            0x000050,  // deep blue sea
            0x0000A0,  // blue sea
            0x4040A0,  // shallows
            0x007070,  // marsh
            0x000E00,  // grass
            0x004700,  // brush
            0x787810,  // desert
            0x307000,  // scrub forest
            0x408300,  // forest
            0x448A00,  // deep forest
            0x444844,  // hills
            0x8F8F8E,  // mountains
            0xAFAFAE,  // high peak
            0x333833,  // low hills
            0x333833,  // low hills
          };
          int color;
          if (datum <= terraColors.length)
            color = terraColors[datum-1];
          else if (datum == 28)  // desert oasis
            color = 0x505060;
          else if (datum == 30 || datum == 31) // desert edge
            color = 0x454C08;
          else if (datum >= 32 && datum <= 38) // road
            color = 0x303800;
          else if (datum == 47)  // desert cactus
            color = 0x689800;
          else if (datum >= 48 && datum <= 51) // straight coast
            color = terraColors[4]; // i.e. grass
          else if (datum >= 52 && datum <= 55) // diagonal coast
            color = 0x0E1E35;
          else if (datum == 71)  // docks
            color = 0x707840;
          else if (datum == 106 || datum == 107) // bridge over troubled water
            color = 0x484F18;
          else if (datum >= 96 && datum <= 111)  // river
            color = 0x4050B0;
          else if (datum == 137) // cross (underworld)
            color = 0x74740F;
          else if (datum == 143) // a'a-pahoehoe amalgam
            color = 0xC00000;
          else if (datum == 164) // sign
            color = 0xBFCABF;
          else if (datum == 212) // cascade
            color = 0x5854B8;
          else if (datum == 222) // blue flame thing (underworld)
            color = 0x1010FF;
          else if (datum == 255) // utter darkness (underworld)
            color = 0x000000;
          else                   // town or some shit
            color = 0xFFFFFF;
          image.setRGB(mapX, mapY, color);
        }

        if (worldJs != null)
          worldJs.write(datum + ",");
      }
      if (worldJs != null)
        worldJs.write("],\n");
      System.out.print(".");
    }

    filename = underworld ? underworldFilename : britanniaFilename;
    if (stitch) {
      filename = String.format(worldStitchedFormat, filename);
    } else {
      filename = String.format(worldMinimapFormat, filename);
    }
    filename = filename.toLowerCase();
    writeImage(image, filename);
    System.out.println("Wrote " + filename);
    if (worldJs != null) {
      worldJs.write("];\n");
      worldJs.close();
    }

    System.out.println(".");
  }

	private static void parseWorldChunks(boolean underworld) {
		// Get tile images
		BufferedImage tiles = getTileImages();
		
		// Open the file...
		String dataFilename = underworld ? underworldFilename : britanniaFilename;
		dataFilename = String.format(worldDataFormat, dataFilename);
		byte[] data = new byte[MAP_SIZE * MAP_SIZE];
		getData(data, getInputStream(dataFilename), true);

		// The map is arranged in chunks of 16x16 tiles.  Underworld has 256.
		// Aboveground has 205 (variously sized rows--skips chunks of all 01s)
		int datum = 0;
		int i = 0;
		int tileX = 0, tileY = 0;
		int mapX, mapY = 0;
		final int chunkCount = (underworld ? 256 : 205);
		for (int chunk = 0; chunk < chunkCount; ++chunk) {
	    // Prepare the chunk image we will draw to -
			BufferedImage image = new BufferedImage(CHUNK_SIZE * TILE_SIZE,
																							CHUNK_SIZE * TILE_SIZE,
																							BufferedImage.TYPE_INT_RGB);
			
	    for (i = 0; i < CHUNK_SIZE * CHUNK_SIZE; ++i) {
				datum = unsign(data[i + chunk * (CHUNK_SIZE * CHUNK_SIZE)]);

				tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
				tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;
				mapX = (i % CHUNK_SIZE) * TILE_SIZE;
				mapY = (i / CHUNK_SIZE) * TILE_SIZE;

				copyTile(tiles, image, tileX, tileY, mapX, mapY, TILE_SIZE);
	    }

	    // Save image file
			String outFilename = underworld ? underworldFilename : britanniaFilename;
			outFilename = outFilename.toLowerCase();
			outFilename = String.format(worldChunkFormat, outFilename, chunk);
			writeImage(image, outFilename);
			System.out.print(".");
		}
		System.out.println();
	}

  */

  // for poking about
  protected void grindMapData(final String fileName,
                              final int mapSize) throws IOException {
    // Get source images
    BufferedImage tiles = getTileImages();

    // Get data
    //final String dataFileName = fileName + ".DAT";
    byte[] data = new byte[mapSize * mapSize];
    //FileInputStream stream = getInputStream(dataFileName);
    FileInputStream stream = getInputStream(fileName);
    int dataRead = getData(data, stream, false);

    int gridNum = 1;
    do {
      // An image for the next grid's worth of tiles
      BufferedImage image = new BufferedImage(mapSize * TILE_SIZE,
                                              mapSize * TILE_SIZE,
                                              BufferedImage.TYPE_INT_RGB);
      parseTiles(tiles, image, data, 0, mapSize);
      
      // Save image file
      String outFileName = String.format("%s%02d.png", fileName, gridNum++);
      outFileName = outFileName.toLowerCase();
      writeImage(image, outFileName);
      System.out.print(".");

      // Get next batch of data
      dataRead = getData(data, stream, false);
    } while (dataRead == mapSize * mapSize);
    // this will omit the last grid's worth of data, but c'est la vie

    System.out.println();
    stream.close();
  }

  // instead of making a series of squares, just spit out a rectangle as requested
  protected void sliceMapData(final String fileName,
                              final int mapWidth,
                                     final int mapHeight) throws IOException {
    // Get source images
    BufferedImage tiles = getTileImages();

    // Get data
    //final String dataFileName = fileName + ".DAT";
    byte[] data = new byte[mapWidth];
    //FileInputStream stream = getInputStream(dataFileName);
    FileInputStream stream = getInputStream(fileName);
    int dataRead = getData(data, stream, false);

    BufferedImage image = new BufferedImage(mapWidth * TILE_SIZE,
                                            mapHeight * TILE_SIZE,
                                            BufferedImage.TYPE_INT_RGB);

    for (int y = 0; y < mapHeight; y++) {
      int mapY = y * TILE_SIZE;
      for (int x = 0; x < dataRead; x++) {
        int mapX = x * TILE_SIZE;
        int datum = unsign(data[x]);
        int tileX = (datum % TEXTURE_TILE_WIDTH) * TILE_SIZE;
        int tileY = (datum / TEXTURE_TILE_WIDTH) * TILE_SIZE;

        copyTile(tiles, image, tileX, tileY, mapX, mapY, 16);
      }

      dataRead = getData(data, stream, false);
      if (dataRead < mapWidth)
        break;
    }
    stream.close();

    // Save image file
    String outFileName = String.format("%s.png", fileName);
    outFileName = outFileName.toLowerCase();
    writeImage(image, outFileName);
  }

  protected void printDataRuns(final String fileName) throws IOException {
    // Maybe also a mode for runs of particular values?

    int runLength = 0;
    int lastDatum = -1; // cross my fingers for first time
    int lastRunLength = -1;
    int datum = 0;

    byte[] data = new byte[1024];
    //FileInputStream stream = getInputStream(dataFileName);
    FileInputStream stream = getInputStream(fileName);
    int dataRead = 0;

    do {
      dataRead = getData(data, stream, false);

      for (int i = 0; i < dataRead; ++i) {
        datum = unsign(data[i]);
        if ((datum == 0) != (lastDatum == 0)) {
          if (runLength > 0) {
            if (lastDatum != 0)
              System.out.print("X " + runLength + "\t\t");
            else
              System.out.println("0 " + runLength + "\t\t= " + (lastRunLength + runLength));
          }
          lastRunLength = runLength;
          runLength = 1;
          lastDatum = datum;
        } else {
          runLength++;
        }
      }

    } while (dataRead > 0);

    System.out.println();
    stream.close();
  }

  protected boolean eatTestingArgs(String[] args) throws IOException {
    final String modeArg = args[0].toLowerCase();

    if (modeArg.equals("-grind") && args.length > 2) {
      grindMapData(args[1], new Integer(args[2]).intValue());
      return true;
    }
    else if (modeArg.equals("-slice") && args.length > 3) {
      sliceMapData(args[1],
                   new Integer(args[2]).intValue(),
                   new Integer(args[3]).intValue());
      return true;
    }
    else if (modeArg.equals("-run") && args.length > 1) {
      printDataRuns(args[1].toUpperCase());
      return true;
    }
    return false;
  }
}
