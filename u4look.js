// I just eyeballed these and drew the descriptions from the U5 ones.
// Where are they for U4?
var u4look = {
  0: "deep water",
  1: "water",
  2: "shoals",
  3: "marsh",
  4: "grass",
  5: "brush",
  6: "forest",
  7: "hills",
  8: "mountains",
  9: "a dungeon",
  10: "a towne",
  11: "a mighty castle",
  12: "a village",
  13: "a mighty castle",
  14: "a mighty castle",
  15: "a mighty castle",
  23: "a bridge",
  29: "ruins",
  30: "a mystic shrine",
  61: "white stone",
  70: "molten lava",
  76: "molten lava"
};
